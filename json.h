#ifndef CTP_JSON_H
#define CTP_JSON_H

#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <iterator>
#include <limits>
#include <memory>
#include <istream>
#include <ostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace ctp
{
  // UTF-8 ITERATOR

  template<typename charT> class utf8_iterator;

  class utf8_iterator_base
  {
  public:
    utf8_iterator_base(std::string::const_iterator) noexcept;

    utf8_iterator_base() noexcept = default;
    utf8_iterator_base &operator++() noexcept;
    utf8_iterator_base operator++(int) noexcept;
    utf8_iterator_base &operator--() noexcept;
    utf8_iterator_base operator--(int) noexcept;

    bool operator==(const utf8_iterator_base &) const noexcept;
    bool operator!=(const utf8_iterator_base &) const noexcept;

    template<typename charT> friend class utf8_iterator;

  private:
    std::string::const_iterator m_it;
  };

  // char -> ASCII; unsigned char -> Latin-1; char16_t -> BMP; char32_t -> full Unicode
  template<typename charT> class utf8_iterator: public utf8_iterator_base
  {
  public:
    using difference_type = std::ptrdiff_t;
    using value_type = charT;
    using pointer = void;
    using reference = charT;
    using iterator_category = std::bidirectional_iterator_tag;

    using utf8_iterator_base::utf8_iterator_base;
    utf8_iterator(const utf8_iterator_base &) noexcept;

    charT operator*() const noexcept;
    utf8_iterator &operator++() noexcept;
    utf8_iterator operator++(int) noexcept;
    utf8_iterator &operator--() noexcept;
    utf8_iterator operator--(int) noexcept;

    using utf8_iterator_base::operator==;
    using utf8_iterator_base::operator!=;
  };

  template<> char utf8_iterator<char>::operator*() const noexcept;
  template<> unsigned char utf8_iterator<unsigned char>::operator*() const noexcept;
  //template<> wchar_t utf8_iterator<wchar_t>::operator*() const noexcept;  // TODO
  template<> char16_t utf8_iterator<char16_t>::operator*() const noexcept;
  template<> char32_t utf8_iterator<char32_t>::operator*() const noexcept;


  // SERIALIZATION/DESERIALIZATION

  class json;

  template<typename charT> std::basic_ostream<charT> &
    operator<<(std::basic_ostream<charT> &, const json &);

  std::ios_base &json_ascii(std::ios_base &) noexcept;
  std::ios_base &json_bmp(std::ios_base &) noexcept;
  std::ios_base &json_unicode(std::ios_base &) noexcept;  // default
  std::ios_base &json_javascript_compat(std::ios_base &) noexcept;
  std::ios_base &json_no_javascript_compat(std::ios_base &) noexcept;  // default
  std::ios_base &json_printable_only(std::ios_base &) noexcept;
  std::ios_base &json_no_printable_only(std::ios_base &) noexcept;  // default

  template<typename charT> std::basic_istream<charT> &
    operator>>(std::basic_istream<charT> &, json &);

  std::ios_base &json_reject_duplicate_keys(std::ios_base &) noexcept;
  std::ios_base &json_no_reject_duplicate_keys(std::ios_base &) noexcept;  // default


  // JSON TYPE

  class json_null { };


  class json_boolean
  {
  public:
    json_boolean(bool = false) noexcept;
    operator bool() const noexcept;

    json_boolean &operator=(bool) noexcept;

  private:
    bool m_boolean;
  };


  struct bits_t { }; constexpr bits_t bits { };

  class json_number
  {
  public:
    json_number(int = 0);
    json_number(long);
    json_number(long long);
    json_number(unsigned);
    json_number(unsigned long);
    json_number(unsigned long long);
    // Optionally limit total significant (decimal) digits.
    json_number(float, int sig_digs = std::numeric_limits<float>::max_digits10);
    json_number(double, int sig_digs = std::numeric_limits<double>::max_digits10);
    json_number(long double, int sig_digs = std::numeric_limits<long double>::max_digits10);
    // TODO: constructors with specified precision
    // As above, but ensure that at least the given number of bits are represented.
    json_number(float, bits_t, int sig_bits);
    json_number(double, bits_t, int sig_bits);
    json_number(long double, bits_t, int sig_bits);

    json_number &operator=(int);
    json_number &operator=(long);
    json_number &operator=(long long);
    json_number &operator=(unsigned);
    json_number &operator=(unsigned long);
    json_number &operator=(unsigned long long);
    json_number &operator=(float);
    json_number &operator=(double);
    json_number &operator=(long double);

    // All conversions throw std::out_of_range on numeric overflow.
    // Precision (including fractions when converting to integers) is silently lost.
    // FIXME: should these be explicit, given that they can fail?
    // WORKING: or should we add more? or templatize them? allow "not" to fail?
    // (not failing would be hard for unsigned types)
    // or make a separate templated cast function/method? -> probably this...
    // b/c normal cast semantics for unsigned is modulo-2, which we don't give
    operator int() const;
    operator long() const;
    operator long long() const;
    operator unsigned() const;
    operator unsigned long() const;
    operator unsigned long long() const;
    operator float() const;
    operator double() const;
    operator long double() const;

    friend bool is_integral(const json_number &) noexcept;
    friend bool is_zero(const json_number &) noexcept;
    friend bool is_negative(const json_number &) noexcept;
    friend int significant_digits(const json_number &) noexcept;
    friend int significant_bits(const json_number &) noexcept;
    friend int integral_digits(const json_number &) noexcept;
    friend int integral_bits(const json_number &) noexcept;

  private:
    std::string m_number;

    json_number(const std::string &);
    json_number(std::string &&) noexcept;

    template<typename charT> friend std::basic_ostream<charT> &operator<<(std::basic_ostream<charT> &, const json &);
    template<typename charT> friend std::basic_istream<charT> &operator>>(std::basic_istream<charT> &, json &);
  };

  // Does the value have integral value (i.e., no non-zero fraction).
  bool is_integral(const json_number &) noexcept;
  // Is value 0 or negative 0.
  bool is_zero(const json_number &) noexcept;
  // Is value less than 0, or negative zero.
  bool is_negative(const json_number &) noexcept;
  // Number of (decimal) digits which form significant portion of the value.
  // (Excluding leading zeros; including trailing zeros.)
  int significant_digits(const json_number &) noexcept;
  // Approximate upper bound of the number of bits which form significant
  // portion of the value.  (Excluding leading zeros; including trailing zeros.)
  // TODO: should this be lower bound, to ensure correct round-tripping through constructor?
  int significant_bits(const json_number &) noexcept;
  // Number of (decimal) digits which form the integer portion of the value.
  // 0 for absolute values less than 1.
  int integral_digits(const json_number &) noexcept;
  // Approximate upper bound of the number of bits which form
  // the integer portion of the value.  0 for absolute values less than 1.
  int integral_bits(const json_number &) noexcept;
  // TODO: precision_digits/precision_bits
  //       = place of least significant digit/bit
  // TODO: return significand as float/double/long double (base 2???)
  // TODO: add exponent_digits/bits for use with significand function...
  //       (can't replace integral_bits, since exponent_bits is much more
  //       expensive to calculate)
  //       maybe overload frexp?


  template<typename charT> class json_string_ops;
  template<typename charT> class const_json_string_ops;

  class json_string
  {
  public:
    json_string() = default;
    json_string(const char *);
    json_string(const char *, std::size_t);
    json_string(const std::string &);
    json_string(std::string &&) noexcept;

    bool empty() const noexcept;
    void clear() noexcept;

    json_string &operator=(const char *);
    json_string &operator=(const std::string &);
    json_string &operator=(std::string &&) noexcept;

    // NOTE: May be changed to a plain std::string.  Do NOT expect pointers
    // to the returned std::string to remain valid just because you are
    // holding on to the json_string.
    operator const std::string &() const noexcept;  // UTF-8
    operator std::string &&() && noexcept;

    template<typename charT> json_string_ops<charT> ops() noexcept;
    template<typename charT> const_json_string_ops<charT> ops() const noexcept;

    template<typename charT> friend class json_string_ops;
    template<typename charT> friend class const_json_string_ops;

  private:
    std::string m_string;
  };

  template<typename charT> class json_string_ops
  {
  public:
    using value_type = charT;
    using const_iterator = utf8_iterator<charT>;

    bool empty() const noexcept;
    void clear() const noexcept;

    void push_back(charT) const;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    friend class json_string;

  private:
    json_string_ops(json_string &) noexcept;

    json_string &m_json_string;
  };

  template<> void json_string_ops<char>::push_back(char) const;

  template<typename charT> class const_json_string_ops
  {
  public:
    using value_type = charT;
    using const_iterator = utf8_iterator<charT>;

    const_json_string_ops(json_string_ops<charT>) noexcept;

    bool empty() const noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    friend class json_string;

  private:
    const_json_string_ops(const json_string &) noexcept;

    const json_string &m_json_string;
  };


  class json_array
  {
  public:
    using value_type = json;

    class const_iterator
    {
    public:
      using difference_type = std::ptrdiff_t;
      using value_type = json;
      using pointer = const value_type *;
      using reference = const value_type &;
      using iterator_category = std::bidirectional_iterator_tag;

      const_iterator() noexcept = default;

      reference operator*() const noexcept;
      pointer operator->() const noexcept;
      const_iterator &operator++() noexcept;
      const_iterator operator++(int) noexcept;
      const_iterator &operator--() noexcept;
      const_iterator operator--(int) noexcept;

      bool operator==(const const_iterator &) const noexcept;
      bool operator!=(const const_iterator &) const noexcept;

    private:
      const_iterator(std::vector<json>::const_iterator) noexcept;
      std::vector<json>::const_iterator m_it;
      friend class json_array;
    };

    json_array() = default;
    json_array(std::initializer_list<json>);

    bool empty() const noexcept;
    void clear() noexcept;

    void push_back(const json &);
    void push_back(json &&);
    json &back() noexcept;
    const json &back() const noexcept;
    template<typename... Args> void emplace_back(Args &&...);

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

  private:
    std::vector<json> m_array;
  };


  class json_object
  {
  public:
    using value_type = std::pair<const std::string, json>;

    class iterator;

    class const_iterator
    {
    public:
      using difference_type = std::ptrdiff_t;
      using value_type = std::pair<const std::string, json>;
      using pointer = const value_type *;
      using reference = const value_type &;
      using iterator_category = std::bidirectional_iterator_tag;

      const_iterator() noexcept = default;
      const_iterator(iterator) noexcept;

      reference operator*() const noexcept;
      pointer operator->() const noexcept;
      const_iterator &operator++() noexcept;
      const_iterator operator++(int) noexcept;
      const_iterator &operator--() noexcept;
      const_iterator operator--(int) noexcept;

      bool operator==(const const_iterator &) const noexcept;
      bool operator!=(const const_iterator &) const noexcept;

    private:
      const_iterator(std::vector<std::pair<const std::string, json>>::const_iterator) noexcept;
      std::vector<std::pair<const std::string, json>>::const_iterator m_it;
      friend class json_object;
      friend class iterator;
    };

    class iterator
    {
    public:
      using difference_type = std::ptrdiff_t;
      using value_type = std::pair<const std::string, json>;
      using pointer = value_type *;
      using reference = value_type &;
      using iterator_category = std::output_iterator_tag;

      iterator() noexcept = default;

      reference operator*() const noexcept;
      pointer operator->() const noexcept;
      iterator &operator++() noexcept;
      iterator operator++(int) noexcept;

      bool operator==(const iterator &) const noexcept;
      bool operator!=(const iterator &) const noexcept;

    private:
      iterator(std::vector<std::pair<const std::string, json>>::iterator) noexcept;
      std::vector<std::pair<const std::string, json>>::iterator m_it;
      friend class json_object;
      friend class const_iterator;
    };

    json_object() = default;
    json_object(std::initializer_list<std::pair<std::string, json>>);

    json_object &operator=(const json_object &other);

    bool empty() const noexcept;
    void clear() noexcept;

    iterator insert(const std::pair<const std::string, json> &);
    iterator insert(std::pair<const std::string, json> &&);
    template<typename... Args> iterator emplace(Args &&...);

    // For compatibility with std::insert_iterator; const_iterator argument is ignored.
    iterator insert(const_iterator, const std::pair<const std::string, json> &);
    iterator insert(const_iterator, std::pair<const std::string, json> &&);
    template<typename... Args> iterator emplace_hint(const_iterator, Args &&...);

    const_iterator begin() const noexcept;
    iterator end() noexcept;
    const_iterator end() const noexcept;

  private:
    std::vector<std::pair<const std::string, json>> m_object;
  };


  enum class json_type { null, boolean, number, string, array, object };

  template<json_type T> struct json_alternative;
  template<> struct json_alternative<json_type::null> { using type = json_null; };
  template<> struct json_alternative<json_type::boolean> { using type = json_boolean; };
  template<> struct json_alternative<json_type::number> { using type = json_number; };
  template<> struct json_alternative<json_type::string> { using type = json_string; };
  template<> struct json_alternative<json_type::array> { using type = json_array; };
  template<> struct json_alternative<json_type::object> { using type = json_object; };

  template<typename T> struct json_in_place_type_t { };
  template<json_type T> struct json_in_place_alternative_t { };

  namespace json_type_tags
  {
    constexpr json_in_place_type_t<json_null> null { };
    constexpr json_in_place_type_t<json_boolean> boolean { };
    constexpr json_in_place_type_t<json_number> number { };
    constexpr json_in_place_type_t<json_string> string { };
    constexpr json_in_place_type_t<json_array> array { };
    constexpr json_in_place_type_t<json_object> object { };
  }

  template<typename T> struct json_type_alternative;
  template<> struct json_type_alternative<json_null> { static constexpr json_type value = json_type::null; };
  template<> struct json_type_alternative<json_boolean> { static constexpr json_type value = json_type::boolean; };
  template<> struct json_type_alternative<json_number> { static constexpr json_type value = json_type::number; };
  template<> struct json_type_alternative<json_string> { static constexpr json_type value = json_type::string; };
  template<> struct json_type_alternative<json_array> { static constexpr json_type value = json_type::array; };
  template<> struct json_type_alternative<json_object> { static constexpr json_type value = json_type::object; };

  class bad_json_access: public std::exception
  {
  public:
    bad_json_access() noexcept = default;
  };

  class json
  {
  public:
    json() noexcept;
    json(json_type);

    json(json_null) noexcept;
    json(json_boolean) noexcept;
    json(const json_number &);
    json(json_number &&) noexcept;
    json(const json_string &);
    json(json_string &&) noexcept;
    json(const json_array &);
    json(json_array &&) noexcept;
    json(const json_object &);
    json(json_object &&) noexcept;

    template<typename T, typename... Args> json(json_in_place_type_t<T>, Args &&...);
    template<typename T> json(json_in_place_type_t<T>, std::initializer_list<json>);
    template<json_type T, typename... Args> json(json_in_place_alternative_t<T>, Args &&...);
    template<json_type T> json(json_in_place_alternative_t<T>, std::initializer_list<json>);

    json(bool) noexcept;
    json(int);
    json(long);
    json(long long);
    json(unsigned);
    json(unsigned long);
    json(unsigned long long);
    json(float);
    json(double);
    json(long double);
    json(const char *);
    json(const std::string &);
    json(std::string &&) noexcept;

    json &operator=(json_null) noexcept;
    json &operator=(json_boolean) noexcept;
    json &operator=(const json_number &);
    json &operator=(json_number &&) noexcept;
    json &operator=(const json_string &);
    json &operator=(json_string &&) noexcept;
    json &operator=(const json_array &);
    json &operator=(json_array &&) noexcept;
    json &operator=(const json_object &);
    json &operator=(json_object &&) noexcept;

    json &operator=(bool) noexcept;
    json &operator=(int);
    json &operator=(long);
    json &operator=(long long);
    json &operator=(unsigned);
    json &operator=(unsigned long);
    json &operator=(unsigned long long);
    json &operator=(float);
    json &operator=(double);
    json &operator=(long double);
    json &operator=(const char *);
    json &operator=(const std::string &);
    json &operator=(std::string &&) noexcept;

    json(const json &);
    json(json &&) noexcept;
    json &operator=(const json &);
    json &operator=(json &&) noexcept;
    ~json();

    json_type type() const noexcept;
    bool is_null() const noexcept;

    template<typename T, typename... Args> T &emplace(Args &&...);
    template<typename T> T &emplace(std::initializer_list<json>);
    template<json_type T, typename... Args> typename json_alternative<T>::type &emplace(Args &&...);
    template<json_type T> typename json_alternative<T>::type &emplace(std::initializer_list<json>);

    template<typename T> friend T &get(json &);
    template<typename T> friend T &&get(json &&);
    template<typename T> friend const T &get(const json &);
    template<typename T> friend const T &&get(const json &&);

    template<typename T> friend T *get_if(json *) noexcept;
    template<typename T> friend const T *get_if(const json *) noexcept;

  private:
    json_type m_type;

    union ju
    {
      ju() noexcept;
      ju(json_null) noexcept;
      ju(json_boolean) noexcept;
      ju(const json_number &);
      ju(json_number &&) noexcept;
      ju(const json_string &);
      ju(json_string &&) noexcept;
      ju(const json_array &);
      ju(json_array &&) noexcept;
      ju(const json_object &);
      ju(json_object &&) noexcept;
      ~ju() { }
      
      json_null m_null { };
      json_boolean m_boolean;
      json_number m_number;
      json_string m_string;
      json_array m_array;
      json_object m_object;
    } m_union;
  };

  template<typename T> bool holds_alternative(const json &) noexcept;

  template<json_type T> typename json_alternative<T>::type &get(json &);
  template<json_type T> typename json_alternative<T>::type &&get(json &&);
  template<json_type T> const typename json_alternative<T>::type &get(const json &);
  template<json_type T> const typename json_alternative<T>::type &&get(const json &&);

  template<json_type T> typename json_alternative<T>::type *get_if(json *) noexcept;
  template<json_type T> const typename json_alternative<T>::type *get_if(const json *) noexcept;
}


// PRIVATE

namespace ctp
{
  // UTF-8 ITERATOR

  inline utf8_iterator_base::utf8_iterator_base(const std::string::const_iterator it) noexcept
    : m_it{it} { }
  inline utf8_iterator_base utf8_iterator_base::operator++(int) noexcept
  { const utf8_iterator_base ret = *this; ++*this; return ret; }
  inline utf8_iterator_base utf8_iterator_base::operator--(int) noexcept
  { const utf8_iterator_base ret = *this; --*this; return ret; }
  inline bool utf8_iterator_base::operator==(const utf8_iterator_base &other) const noexcept
  { return m_it == other.m_it; }
  inline bool utf8_iterator_base::operator!=(const utf8_iterator_base &other) const noexcept
  { return m_it != other.m_it; }

  template<typename charT> inline utf8_iterator<charT>::utf8_iterator(const utf8_iterator_base &it) noexcept
    : utf8_iterator_base{it} { }
  template<typename charT> inline utf8_iterator<charT> &utf8_iterator<charT>::operator++() noexcept
  { utf8_iterator_base::operator++(); return *this; }
  template<typename charT> inline utf8_iterator<charT> utf8_iterator<charT>::operator++(const int x) noexcept
  { return utf8_iterator_base::operator++(x); }
  template<typename charT> inline utf8_iterator<charT> &utf8_iterator<charT>::operator--() noexcept
  { utf8_iterator_base::operator--(); return *this; }
  template<typename charT> inline utf8_iterator<charT> utf8_iterator<charT>::operator--(const int x) noexcept
  { return utf8_iterator_base::operator--(x); }


  // JSON TYPE

  inline json_boolean::json_boolean(const bool v) noexcept: m_boolean{v} { }
  inline json_boolean::operator bool() const noexcept { return m_boolean; }
  inline json_boolean &json_boolean::operator=(const bool v) noexcept { m_boolean = v; return *this; }

  inline json_number::json_number(const int v): m_number{std::to_string(v)} { }
  inline json_number::json_number(const long v): m_number{std::to_string(v)} { }
  inline json_number::json_number(const long long v): m_number{std::to_string(v)} { }
  inline json_number::json_number(const unsigned v): m_number{std::to_string(v)} { }
  inline json_number::json_number(const unsigned long v): m_number{std::to_string(v)} { }
  inline json_number::json_number(const unsigned long long v): m_number{std::to_string(v)} { }
  inline json_number &json_number::operator=(const int v) { m_number = std::to_string(v); return *this; } 
  inline json_number &json_number::operator=(const long v) { m_number = std::to_string(v); return *this; }
  inline json_number &json_number::operator=(const long long v) { m_number = std::to_string(v); return *this; }
  inline json_number &json_number::operator=(const unsigned v) { m_number = std::to_string(v); return *this; }
  inline json_number &json_number::operator=(const unsigned long v) { m_number = std::to_string(v); return *this; }
  inline json_number &json_number::operator=(const unsigned long long v) { m_number = std::to_string(v); return *this; }
  inline bool is_negative(const json_number &jn) noexcept { return jn.m_number[0] == '\x2D'; }
  inline json_number::json_number(const std::string &s): m_number{s} { }
  inline json_number::json_number(std::string &&s) noexcept: m_number{std::move(s)} { }

  inline json_string::json_string(const char *const p): m_string(p) { }
  inline json_string::json_string(const char *const p, const std::size_t s): m_string(p, s) { }
  inline json_string::json_string(const std::string &v): m_string{v} { }
  inline json_string::json_string(std::string &&v) noexcept: m_string{std::move(v)} { }
  inline bool json_string::empty() const noexcept { return m_string.empty(); }
  inline void json_string::clear() noexcept { m_string.clear(); }
  inline json_string &json_string::operator=(const char *const p) { m_string = p; return *this; }
  inline json_string &json_string::operator=(const std::string &v) { m_string = v; return *this; }
  inline json_string &json_string::operator=(std::string &&v) noexcept { m_string = std::move(v); return *this; }
  inline json_string::operator const std::string &() const noexcept { return m_string; }
  inline json_string::operator std::string &&() && noexcept { return std::move(m_string); }
  template<typename charT> inline json_string_ops<charT> json_string::ops() noexcept
  { return json_string_ops<charT>(*this); }
  template<typename charT> inline const_json_string_ops<charT> json_string::ops() const noexcept
  { return const_json_string_ops<charT>(*this); }

  template<typename charT> inline bool json_string_ops<charT>::empty() const noexcept
  { return m_json_string.empty(); }
  template<typename charT> inline void json_string_ops<charT>::clear() const noexcept
  { m_json_string.clear(); }
  template<typename charT> inline typename json_string_ops<charT>::const_iterator
    json_string_ops<charT>::begin() const noexcept
  { return utf8_iterator<charT>(m_json_string.m_string.begin()); }
  template<typename charT> inline typename json_string_ops<charT>::const_iterator
    json_string_ops<charT>::end() const noexcept
  { return utf8_iterator<charT>(m_json_string.m_string.end()); }
  template<typename charT> inline json_string_ops<charT>::json_string_ops(json_string &js) noexcept
    : m_json_string{js} { }
  template<> inline void json_string_ops<char>::push_back(const char v) const { m_json_string.m_string.push_back(v); }

  template<typename charT> inline const_json_string_ops<charT>::const_json_string_ops(const json_string_ops<charT> ops) noexcept
    : m_json_string{ops.m_json_string} { }
  template<typename charT> inline bool const_json_string_ops<charT>::empty() const noexcept
  { return m_json_string.empty(); }
  template<typename charT> inline typename const_json_string_ops<charT>::const_iterator
    const_json_string_ops<charT>::begin() const noexcept
  { return utf8_iterator<charT>(m_json_string.m_string.begin()); }
  template<typename charT> inline typename const_json_string_ops<charT>::const_iterator
    const_json_string_ops<charT>::end() const noexcept
  { return utf8_iterator<charT>(m_json_string.m_string.end()); }
  template<typename charT> inline const_json_string_ops<charT>::const_json_string_ops(const json_string &js) noexcept
    : m_json_string{js} { }

  inline json_array::json_array(const std::initializer_list<json> il): m_array(il) { }
  inline bool json_array::empty() const noexcept { return m_array.empty(); }
  inline void json_array::clear() noexcept { m_array.clear(); }
  inline void json_array::push_back(const json &v) { m_array.push_back(v); }
  inline void json_array::push_back(json &&v) { m_array.push_back(std::move(v)); }
  inline json &json_array::back() noexcept { return m_array.back(); }
  inline const json &json_array::back() const noexcept { return m_array.back(); }
  template<typename... Args> inline void json_array::emplace_back(Args &&... args)
  { m_array.emplace_back(std::forward<Args>(args)...); }
  inline json_array::const_iterator json_array::begin() const noexcept
  { return const_iterator(m_array.begin()); }
  inline json_array::const_iterator json_array::end() const noexcept
  { return const_iterator(m_array.end()); }

  inline json_array::const_iterator::reference json_array::const_iterator::operator*() const noexcept { return *m_it; }
  inline json_array::const_iterator::pointer json_array::const_iterator::operator->() const noexcept { return m_it.operator->(); }
  inline json_array::const_iterator &json_array::const_iterator::operator++() noexcept { ++m_it; return *this; }
  inline json_array::const_iterator json_array::const_iterator::operator++(int) noexcept { return const_iterator(m_it++); }
  inline json_array::const_iterator &json_array::const_iterator::operator--() noexcept { --m_it; return *this; }
  inline json_array::const_iterator json_array::const_iterator::operator--(int) noexcept { return const_iterator(m_it--); }
  inline bool json_array::const_iterator::operator==(const const_iterator &other) const noexcept { return m_it == other.m_it; }
  inline bool json_array::const_iterator::operator!=(const const_iterator &other) const noexcept { return m_it != other.m_it; }
  inline json_array::const_iterator::const_iterator(std::vector<json>::const_iterator it) noexcept
    : m_it{std::move(it)} { }

  inline json_object::json_object(const std::initializer_list<std::pair<std::string, json>> il)
  { for (auto p: il) m_object.emplace_back(p.first, p.second); }
  inline bool json_object::empty() const noexcept { return m_object.empty(); }
  inline void json_object::clear() noexcept { m_object.clear(); }
  inline json_object::iterator json_object::insert(const std::pair<const std::string, json> &nv)
  { m_object.push_back(nv); return --m_object.end(); }
  inline json_object::iterator json_object::insert(std::pair<const std::string, json> &&nv)
  { m_object.push_back(std::move(nv)); return --m_object.end(); }
  template<typename... Args> inline json_object::iterator json_object::emplace(Args &&... args)
  { m_object.emplace_back(std::forward<Args>(args)...); return --m_object.end(); }
  inline json_object::iterator json_object::insert(const_iterator, const std::pair<const std::string, json> &nv)
  { return insert(nv); }
  inline json_object::iterator json_object::insert(const_iterator, std::pair<const std::string, json> &&nv)
  { return insert(std::move(nv)); }
  template<typename... Args> inline json_object::iterator json_object::emplace_hint(const_iterator, Args &&... args)
  { return emplace(std::forward<Args>(args)...); }
  inline json_object::const_iterator json_object::begin() const noexcept { return m_object.begin(); }
  inline json_object::iterator json_object::end() noexcept { return m_object.end(); }
  inline json_object::const_iterator json_object::end() const noexcept { return m_object.end(); }

  inline json_object::const_iterator::reference json_object::const_iterator::operator*() const noexcept { return *m_it; }
  inline json_object::const_iterator::pointer json_object::const_iterator::operator->() const noexcept { return m_it.operator->(); }
  inline json_object::const_iterator &json_object::const_iterator::operator++() noexcept { ++m_it; return *this; }
  inline json_object::const_iterator json_object::const_iterator::operator++(int) noexcept { return const_iterator(m_it++); }
  inline json_object::const_iterator &json_object::const_iterator::operator--() noexcept { --m_it; return *this; }
  inline json_object::const_iterator json_object::const_iterator::operator--(int) noexcept { return const_iterator(m_it--); }
  inline bool json_object::const_iterator::operator==(const const_iterator &other) const noexcept { return m_it == other.m_it; }
  inline bool json_object::const_iterator::operator!=(const const_iterator &other) const noexcept { return m_it != other.m_it; }
  inline json_object::const_iterator::const_iterator(std::vector<std::pair<const std::string, json>>::const_iterator it) noexcept
    : m_it{std::move(it)} { }
  inline json_object::const_iterator::const_iterator(iterator it) noexcept
    : m_it{std::move(it.m_it)} { }

  inline json_object::iterator::reference json_object::iterator::operator*() const noexcept { return *m_it; }
  inline json_object::iterator::pointer json_object::iterator::operator->() const noexcept { return m_it.operator->(); }
  inline json_object::iterator &json_object::iterator::operator++() noexcept { ++m_it; return *this; }
  inline json_object::iterator json_object::iterator::operator++(int) noexcept { return iterator(m_it++); }
  inline bool json_object::iterator::operator==(const iterator &other) const noexcept { return m_it == other.m_it; }
  inline bool json_object::iterator::operator!=(const iterator &other) const noexcept { return m_it != other.m_it; }
  inline json_object::iterator::iterator(std::vector<std::pair<const std::string, json>>::iterator it) noexcept
    : m_it{std::move(it)} { }

  inline json::ju::ju() noexcept { }
  inline json::ju::ju(json_null) noexcept { }
  inline json::ju::ju(const json_boolean v) noexcept: m_boolean{v} { }
  inline json::ju::ju(const json_number &v): m_number{v} { }
  inline json::ju::ju(json_number &&v) noexcept: m_number{std::move(v)} { }
  inline json::ju::ju(const json_string &v): m_string{v} { }
  inline json::ju::ju(json_string &&v) noexcept: m_string{std::move(v)} { }
  inline json::ju::ju(const json_array &v): m_array(v) { }
  inline json::ju::ju(json_array &&v) noexcept: m_array(std::move(v)) { }
  inline json::ju::ju(const json_object &v): m_object(v) { }
  inline json::ju::ju(json_object &&v) noexcept: m_object(std::move(v)) { }

  inline json::json() noexcept: m_type{json_type::null} { }
  inline json::json(json_null) noexcept: m_type{json_type::null} { }
  inline json::json(const json_boolean v) noexcept: m_type{json_type::boolean}, m_union{v} { }
  inline json::json(const json_number &v): m_type{json_type::number}, m_union{v} { }
  inline json::json(json_number &&v) noexcept: m_type{json_type::number}, m_union{std::move(v)} { }
  inline json::json(const json_string &v): m_type{json_type::string}, m_union{v} { }
  inline json::json(json_string &&v) noexcept: m_type{json_type::string}, m_union{std::move(v)} { }
  inline json::json(const json_array &v): m_type{json_type::array}, m_union{v} { }
  inline json::json(json_array &&v) noexcept: m_type{json_type::array}, m_union{std::move(v)} { }
  inline json::json(const json_object &v): m_type{json_type::object}, m_union{v} { }
  inline json::json(json_object &&v) noexcept: m_type{json_type::object}, m_union{std::move(v)} { }

  template<typename T, typename... Args> inline json::json(json_in_place_type_t<T>, Args &&... args)
    : m_type{json_type_alternative<T>::value}
  { new(reinterpret_cast<T *>(&m_union)) T(std::forward<Args>(args)...); }
  template<typename T> inline json::json(json_in_place_type_t<T>, const std::initializer_list<json> il)
    : m_type{json_type_alternative<T>::value}
  { new(reinterpret_cast<T *>(&m_union)) T(il); }
  template<json_type T, typename... Args> inline json::json(json_in_place_alternative_t<T>, Args &&... args)
    : json(json_in_place_type_t<typename json_alternative<T>::type>(), std::forward<Args>(args)...) { }
  template<json_type T> inline json::json(json_in_place_alternative_t<T>, const std::initializer_list<json> il)
    : json(json_in_place_type_t<typename json_alternative<T>::type>(), il) { }

  inline json::json(const bool v) noexcept: json(json_type_tags::boolean, v) { }
  inline json::json(const int v): json(json_type_tags::number, v) { }
  inline json::json(const long v): json(json_type_tags::number, v) { }
  inline json::json(const long long v): json(json_type_tags::number, v) { }
  inline json::json(const unsigned v): json(json_type_tags::number, v) { }
  inline json::json(const unsigned long v): json(json_type_tags::number, v) { }
  inline json::json(const unsigned long long v): json(json_type_tags::number, v) { }
  inline json::json(const float v): json(json_type_tags::number, v) { }
  inline json::json(const double v): json(json_type_tags::number, v) { }
  inline json::json(const long double v): json(json_type_tags::number, v) { }
  inline json::json(const char *const p): json(json_type_tags::string, p) { }
  inline json::json(const std::string &s): json(json_type_tags::string, s) { }
  inline json::json(std::string &&s) noexcept: json(json_type_tags::string, std::move(s)) { }
  inline json_type json::type() const noexcept { return m_type; }

  template<typename T, typename... Args> inline T &json::emplace(Args &&... args)
  {
    this->~json();
    try { new(this) json(json_in_place_type_t<T>(), std::forward<Args>(args)...); }
    catch (...) { new(this) json; throw; }
    return reinterpret_cast<T &>(m_union);
  }
  template<typename T> inline T &json::emplace(const std::initializer_list<json> il)
  {
    this->~json();
    try { new(this) json(json_in_place_type_t<T>(), il); }
    catch (...) { new(this) json; throw; }
    return reinterpret_cast<T &>(m_union);
  }
  template<json_type T, typename... Args> inline typename json_alternative<T>::type &json::emplace(Args &&... args)
  { return emplace<json_alternative<T>::type>(std::forward<Args>(args)...); }
  template<json_type T> inline typename json_alternative<T>::type &json::emplace(const std::initializer_list<json> il)
  { return emplace<json_alternative<T>::type>(il); }

  inline bool json::is_null() const noexcept { return type() == json_type::null; }

  template<typename T> inline bool holds_alternative(const json &j) noexcept
  { return j.type() == json_type_alternative<T>::value; }

  template<json_type T> inline typename json_alternative<T>::type &get(json &j)
  { return get<typename json_alternative<T>::type>(j); }
  template<json_type T> inline typename json_alternative<T>::type &&get(json &&j)
  { return get<typename json_alternative<T>::type>(j); }
  template<json_type T> inline const typename json_alternative<T>::type &get(const json &j)
  { return get<typename json_alternative<T>::type>(j); }
  template<json_type T> inline const typename json_alternative<T>::type &&get(const json &&j)
  { return get<typename json_alternative<T>::type>(j); }

  template<json_type T> inline typename json_alternative<T>::type *get_if(json *const j) noexcept
  { return get_if<typename json_alternative<T>::type>(j); }
  template<json_type T> inline const typename json_alternative<T>::type *get_if(const json *const j) noexcept
  { return get_if<typename json_alternative<T>::type>(j); }
}

#endif
