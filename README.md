# CTP JSON

A very opinionated JSON library.


## Philosophy

The idea behind this library is that JSON values should not be conflated
with values of the host language.  JSON numbers are not doubles; JSON
objects are not maps; etc.  The reason behind this belief is simple: the
library does not know what is your intended use of the data; therefore any
choice of concrete data structure it makes is almost assuredly wrong.

As an example, one of the most well-known failings caused the traditional
"deep embedding" approach to JSON parsing is JavaScript's inability to
round-trip any JSON which contains numbers outside the range of an IEEE
double.  This is not a fault of JSON: JSON has no concept of IEEE doubles. 
This is not a fault of the application code: the problem exists even if the
application never inspects the offending number, but merely passes it back
to the server.  The fault is that of the deep embedding itself, which
requires that all JSON data be rendered as values of native types.

A similar problem exists with JSON arrays and objects.  What "is" a JSON
array, is it a linked list, a tuple, or something else?  Is a JSON object a
key-value map or a static structure?  Of course these questions are
nonsensical -- JSON values are just octets on a wire; they carry no
intrinsic data layout or access methods.  It is up to the application to
determine how they should best be represented in the host language.

Therefore, this library eschews any notion of "deep embedding", of being
able to access JSON values directly as if they were native data types.  JSON
numbers must first be cast to a native numeric type; JSON arrays can only be
iterated through; etc.  I believe the result of applying this philosophy to
be both easy-to-use, and to promote active understanding of the data itself.


## JSON values

JSON values come in seven flavors, the six JSON datatypes themselves:

* `json_null`
* `json_boolean`
* `json_number`
* `json_string`
* `json_array`
* `json_object`

and finally, the `json` type itself, which represents the union of all the
above.


### `json_null`

`json_null` represents exactly the JSON `null` value.  There is nothing useful
you can do with it except construct it.


### `json_boolean`

`json_boolean` represents either JSON `true` or `false`.  It can be cast to,
or constructed from, `bool` and nothing else, values of which are mapped
to JSON in the obvious manner.


### `json_number`

`json_number` represents any JSON number.  It is limited in magnitude and
precision only by available memory.  `json_number` preserves the following
semantic aspects of JSON numbers:

* sign (including that of values with a numeric value of zero)
* exact (decimal) numeric value
* number of significant (decimal) digits (including trailing zeros)

It does *not* attempt to preserve these aspects:

* absolute exponent value
* case of exponent character
* presence/absence of exponent
* presence/absence of decimal point


#### Inspection

The following functinos are available to directly inspect a `json_number`
without reification, in case it contains information which cannot be
represented as a native numeric value.

`is_integral` indicates whether the value has integral numeric value.  Note
that this will be true of *any* JSON number whose value has a zero fractional
component, *regardless of whether a parsed value contained a decimal point
or exponent*.  If `is_integral` is true, and a cast to an integer succeeds,
the integer represents the same numeric value as the JSON value.

`is_zero` indicates whether the value has a numeric value of zero.  This is
true also for negative zero.

`is_negative` indicates whether the value has a negative numeric value. 
This is true also for negative zero.

`significant_digits` returns the number of significant digits of the number. 
Significant digits are all but leading zeros.  `significant_bits` gives the
same figure in units of bits, rounded up.  If `significant_digits` (resp. 
`significant_bits`) is less than or equal to `std::numeric_limits<T>::digits10`
(resp. `std::numeric_limits<T>::digits`), the number can be cast to `T` without
loss of precision.

Likewise, `integral_digits` returns the number of (significant) digits in
the integral portion of the number, and `integral_bits` gives the same
figure in units of bits, rounded up.  If `integral_digits` (resp.
`integral_bits`) is less than or equal to `std::numeric_limits<intT>::digits10`
(resp. `std::numeric_limits<intT>::digits`), the number can be cast to integer
type `intT` without overflow.  If `integral_digits` (resp. `integral_bits`) is
less than or equal to `std::numeric_limits<floatT>::max_exponent10`
(resp. `std::numeric_limits<floatT>::max_exponent`), the number can be cast
to floating-point type `floatT` without overflow.

There is, by design, no way to perform arithmetic on values of type
`json_number`.


#### Conversion

A `json_number` can be cast to, or constructed from, any of the nine basic
arithmetic types (`int`, `long`, `long long`, `unsigned`, `unsigned long`,
`unsigned long long`, `float`, `double`, and `long double`).  If the JSON
number cannot be represented in the target type, `std::out_of_range` is
thrown.  Precision (including fractions when converting to integers) is
silently lost.

It is recommended to convert an unknown `json_number` first by confirming
that it is in fact integral (if that is what is expected) using `is_integral`,
and then to rely on the cast to throw on an out-of-range value, which exception
you should catch and handle as appropriate for your application.

A `json_number` can additionally be constructed from the floating-point
types with a specified number of significant digits or bits.  By default,
all bits of a floating-point number are considered significant and will be
represented in the formatted JSON.  This is usually not what is intended;
therefore it is a good idea to explicitly specify the number's precision
by using these constructors.


#### Caveats

There is currently no way to directly inspect the value of, or construct
from native types, a `json_number` representing a value outside the bounds
of the native arithmetic types.  Such capability may be added in the future
once a suitable native format is elected.


### `json_string`

`json_string` represents any JSON string.  Recall that JSON strings are
sequences of arbitrary Unicode code points, excluding surrogate pair values. 
`json_string` preserves exactly this information; it does not preserve any
notion of whether an individual code point was represented as an escape
sequence or not.


#### Inspection and Manipulation

The following methods are available to directly inspect or manipulate a
`json_string` without reification.

`empty` indicates whether the value is the empty string.

`clear` resets the value to be the empty string.

A family of functions which operate on the individual code points contained
in a `json_string` are available through the special `ops` method, which is
templated for the following character types:

* `char`, representing the ASCII subset of Unicode
* `unsigned char`, representing the Latin-1 subset of Unicode
* `wchar_t`, representing a platform-dependent subset of Unicode
* `char16_t`, representing the Basic Multilingual Plane
* `char32_t`, representing the full range of Unicode

The instantiations are identical in function, with the exception that code
points outside the range representable in the type are replaced with either
the ASCII C0 SUB character (U+001A), or the Unicode REPLACEMENT CHARACTER
(U+FFFD), as appropriate for the type.

Each instantiation of `ops()` provides the following methods:

`empty` and `clear`, which are identical to the same described above.

`push_back`, which appends to the end of the string the given code point.

`begin` and `end`, which return "pseudo" bidirectional input iterators to
the beginning and past-the-end of the string, respectively.  These
iterators, when dereferenced, return code point values (as opposed to a
reference to some underlying storage).  (In fact the iterators happen to be
of type `utf8_iterator`, which may be used independent of `json_string`;
though this implementation detail is not guaranteed.)

As an example, using the above methods of `ops()`, a `json_string` can be
constructed from a `std::u32string` as follows:

```cpp
json_string from_u32(const std::u32string &s)
{
  json_string ret;
  const auto ret_ops = ret.ops<char32_t>();
  std::copy(s.begin(), s.end(), std::back_inserter(ret_ops));
  return ret;
}
```

And a `std::u32string` can be constructed from a `json_string` like so:

```cpp
std::u32string to_u32(const json_string &js)
{
  return std::u32string(input_json.ops<char32_t>().begin(),
    input_json.ops<char32_t>().end());
}
```

Of course, no direct indexed access to elements of a `json_string` is
provided.  If direct indexed access is required, a `json_string` object
should be converted to, or constructed from, a data structure suited to the
application at hand (such as std::u32string or std::ostringstream).


#### Conversion

A `json_string` can be cast to, or constructed from, a UTF-8 encoded
`std::string`.  (In fact the cast happens to be to a `const std::string &`,
since that is the internal representation of the string, but you should not
rely on this implementation detail.)  Additionally, a `json_string` can be
constructed from a plain (C-style) UTF-8 encoded string which is either
NUL-terminated, or which has an explicit length.  (Note that the easiest way
to construct a JSON string literal is thus `json_string(u8"foobar")`.)

Note that construction from, or access via, iterators over UTF-8 data is not
provided for.  This is to emphasize the nature of JSON strings as containers
of Unicode code points, and not arbitrary octets.  You should instead use
the code-point-specific methods of the `ops` object, described in the
previous section.

Direct construction from, or conversion to, Unicode code point strings is
also not provided for (again in favor of methods of the `ops` object).  This
is both because, when treating strings as opaque blobs, UTF-8 is the most
common format, and because a UTF-8 cast and ASCII cast would conflict with
one another.


### `json_array`

`json_array` represents a JSON array, that is, a finite sequence of JSON
values.  The first (beginning) element in the array corresponds to the
leftmost when serialized; the last (end) element to the rightmost.


#### Inspection and Manipulation

The following methods are the sole means of accessing a `json_array`. 
`json_array` values *cannot* be directly reified to any C++ data type.

`empty` indicates whether the array contains no elements.

`clear` removes all elements from the array.

`push_back` appends a JSON value (of type `json`) to the end (rightmost) of
the array.  `emplace_back` is similar, but constructs a JSON value at the
end of the array from the given arguments (thus avoiding a move).

`back` returns a reference to the last (rightmost) element in the array. 
It is undefined behavior to modify the referenced value after the array has
been modified.  Its primary use is to enable building values in-place.

`begin` and `end` return const bidirectional iterators to the beginning and
past-the-end of the array, respectively.  It is using these iterators that a
`json_array` should be reified to a native array type (e.g.
std::forward_list or std::deque).

As an example, using the above methods, a `json_array` can be constructed from
a `std::vector<int>` as follows:

```cpp
json_array from_vec(const std::vector<int> &v)
{
  json_array ret;
  std::copy(v.begin(), v.end(), std::back_inserter(ret));
  return ret;
}
```

And a `std::vector<json>` can be constructed from a `json_array` like so:

```cpp
std::vector<json> to_vec(const json_array &ja)
{
  return std::vector<json>(ja.begin(), ja.end());
}
```


#### Construction

A `json_array` can be constructed from an initializion list of JSON values. 
Unfortunately, due to a shortcoming of the C++ language, this entails a deep
copy of each element in the list, so it is best avoided in favor of
element-by-element construction using `push_back` or `emplace_back`.


### `json_object`

`json_object` represents a JSON object, that is, a finite collection of
string key--JSON value pairs.  `json_object` does *not* preserve
serialization order, but *does* preserve duplicate keys.


#### Inspection and Manipulation

The following methods are the sole means of accessing a `json_object`. 
`json_object` values *cannot* be directly reified to any C++ data type.

`empty` indicates whether the object contains no key-value pairs.

`clear` removes all key-value pairs from the object.

`insert` adds a key-value pair to the object.  Keys are UTF-8 const
std::string objects; values are JSON values (of type `json`); the pair is a
std::pair of the two.  (If a position at which to insert the key-value pair
is specified, it is ignored.)  `emplace` and `emplace_hint` are similar, but
construct the pair in-place from the given arguments (typically the key and
value themselves).

`insert`, `emplace`, and `emplace_hint` each return a forward output
iterator pointing to the pair just inserted.  This iterator can be used to
manipulate the value just inserted.  It is invalidated after the
`json_object` has been modified.

`begin` and `end` return const bidirectional iterators to a "first" and a
"past-the-end" key-value pair of the object, respectively.  (Since key-value
pairs have no defined order within an object, the iterators traverse a
fictitious order.)  These iterators are invalidated if the object is
modified.  It is using these iterators that a `json_object` should be
reified to a native map type (e.g.  std::unordered_map), or a user-defined
structure type.

As an example, using the above methods, a `json_object` can be constructed from
a `std::map<std::string, int>` as follows:

```cpp
json_object from_map(const std::map<std::string, int> &v)
{
  json_object ret;
  std::copy(v.begin(), v.end(), std::inserter(ret, ret.end()));
  return ret;
}
```

And a `std::map<std::string, json>` can be constructed from a `json_object` like so:

```cpp
std::map<std::string, json> to_map(const json_object &jo)
{
  return std::map<std::string, json>(jo.begin(), jo.end());
}
```


### `json`

`json` represents any JSON value.  It is effectively a union of the above
six JSON datatypes.


#### Inspection and Manipulation

The JSON type of a `json` value can be queried with the `type` method.  As a
shorthand, the `is_null` method checks whether the `json` value is JSON
null.

The datatype value itself can be referenced with the appropriate one of the
six accessor methods `null`, `boolean`, `number`, `string`, `array`, and
`object`.  If the `json` value is not of the appropriate JSON type,
`std::bad_cast` is thrown.  Alternatively, a pointer to the datatype value
can be obtained with one of `null_if`, `boolean_if`, `number_if`,
`string_if`, `array_if`, and `object_if`, which return `nullptr` if called
inappropriately.

The `json` value may be replaced with another JSON value in place,
constructed from the arguments of a call to its `emplace` method.  Note that
if this construction fails (throwing an exception), the `json` value is left
in an indeterminate (but valid) state, as if it were moved from.


#### Conversion

A `json` value can be cast to, or constructed from, any of the six JSON
datatypes.  A default-constructed `json` value represents JSON null.

The constructors and casts of each of the six JSON datatypes are also
available as constructors and casts of the `json` type.  (Note however that
the initialization list constructors for a JSON array and object require a
tag to distinguish them.)  If a `json` value is not of the appropriate JSON
type for a given cast, `std::bad_cast` is thrown (as with the accessor
methods).


## I/O

I/O (serialization and deserialization) is implemented as stream I/O.

The following four character types are supported:

A `char` stream is considered to be a stream of octets of UTF-8 encoded
Unicode code points.  This is usually what you want.  If writing/reading
to/from a file, it is equivalent to, but more efficient than, using the
`std::codecvt_utf8<char32_t>` facet in conjuction with a `char32_t` stream. 
This is also your only option for UTF-8 encoded strings, as the
`std::codecvt` facets are not supported by `std::basic_stringstream` and
friends.

A `wchar_t` stream is considered to be a stream of Unicode code points of
whatever range is supported by `wchar_t` on the host platform.

A `char16_t` stream is considered to be a stream of Unicode code points,
limited to the Basic Multilingual Plane.  Note that this is **not** UTF-16
and such streams do *not* support surrogate pairs.  If UTF-16 I/O is
desired, the `std::codecvt_utf16<char32_t>` facet should be used in
conjuction with a `char32_t` stream.

A `char32_t` stream is considered to be a stream of Unicode code points.

(Note that, while the `json_string` type supports access as a Latin-1
string for purposes of storing legacy data, no such support is provided for
Latin-1 I/O.)


### Serialization

A `json` value may be serialized to a `std::basic_ostream` with the `<<`
operator.  JSON escape sequences will be used where necessary to conform to
any character range limitations of the output stream.


#### Settings

The following manipulators allow setting the JSON output style of an output
stream.

`json_ascii` forces all non-ASCII characters in JSON strings or keys to be
escaped.  This is useful in conjunction with UTF-8 (`char`) streams to
produce ASCII output.

`json_bmp` forces all characters outside the Basic Multilingual Plane in
JSON strings or keys to be escaped.  `json_bmp` is implied by `json_ascii`,
and when writing to a stream which does not support non-BMP code points
(i.e., `char16_t` streams and, on some platforms, `wchar_t` streams).

`json_unicode` disables both `json_ascii` and `json_bmp`.  It is set by
default.

`json_javascript_compat` forces the serialized JSON to be valid
JavaScript/ECMAScript.  It does so by escaping line terminator characters
U+2028 and U+2029 in JSON strings and keys.  (Of course it is never a good
idea to directly evaluate JSON as JavaScript due to security concerns.)
`json_no_javascript_compat` (the default) disables this compatibility mode.

`json_printable_only` restricts the serialized output to consist only of
characters with visual representations.  It does so by escaping non-visual
characters in JSON strings and keys.  Specifically, all so-called "default
ignorable" code points and control characters are escaped.  (Note that
whitespace characters are *not* escaped.)  `json_no_printable_only` (the
default) disables this mode.


#### Caveats

Pretty-printing is currently not supported.


### Deserialization

A `json` value may be deserialized from a `std::basic_istream` with the `>>`
operator.

If deserialization fails, `failbit` is set (or an exception is thrown), and
input consumption halts at the first erroneous character.


#### Settings

The following manipulators allow setting the JSON input format expected on
an input stream.

`json_reject_duplicate_keys` rejects (fails to parse) any JSON value
containing an object which contains duplicate keys.  While such values are
conformant JSON, rejecting them eliminates a class of security bugs relating
to an upstream application ignoring a different subset of duplicate keys
from a downstream application.  `json_no_reject_duplicate_keys` (the
default) disables this mode.  It is possible this default will change in the
future; applications relying on duplicate keys should explicitly disable
this mode.


#### Caveats

`char16_t` and `char32_t` input streams are not supported due to lack of
support on certain platforms (e.g. macOS).
