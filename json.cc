#include <algorithm>
#include <cmath>
#include <cstddef>
#include <ios>
#include <istream>
#include <iterator>
#include <limits>
#include <ostream>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <unordered_set>
#include <utility>
#include <vector>
#include "json.h"

//
// UTF-8 ITERATOR
//

ctp::utf8_iterator_base &ctp::utf8_iterator_base::operator++() noexcept
{
  const unsigned char c = static_cast<unsigned char>(*m_it);
  if (c < 0xC0u) ++m_it;
  else if (c < 0xE0u) m_it += 2;
  else if (c < 0xF0u) m_it += 3;
  else m_it += 4;
  return *this;
}

ctp::utf8_iterator_base &ctp::utf8_iterator_base::operator--() noexcept
{
  do --m_it; while ((static_cast<unsigned char>(*m_it) & 0xC0u) == 0x80u);
  return *this;
}


template<> char ctp::utf8_iterator<char>::operator*() const noexcept
{
  const unsigned char c = static_cast<unsigned char>(*m_it);

  if (c < 0x80u) return c;
  else return '\x1A';
}

template<> unsigned char ctp::utf8_iterator<unsigned char>::operator*() const noexcept
{
  const unsigned char c = static_cast<unsigned char>(*m_it);

  if (c < 0x80u) return c;
  else if (c < 0xC4u)
    return ((c & 0x3u) << 6) | (static_cast<unsigned char>(m_it[1]) & 0x3Fu);
  else return 0x1Au;
}

template<> char16_t ctp::utf8_iterator<char16_t>::operator*() const noexcept
{
  const unsigned char c = static_cast<unsigned char>(*m_it);

  if (c < 0x80u) return c;
  else
  {
    auto it = m_it;
    char16_t d;

    if (c < 0xE0u)
      d = static_cast<char16_t>(c & 0x1Fu) << 6;
    else if (c < 0xF0u)
    {
      d = static_cast<char16_t>(c & 0xFu) << 12;
      ++it;
      d |= static_cast<char16_t>(static_cast<unsigned char>(*it) & 0x3Fu) << 6;
    }
    else return u'\uFFFD';

    ++it;
    d |= static_cast<unsigned char>(*it) & 0x3Fu;

    return d;
  }
}

template<> char32_t ctp::utf8_iterator<char32_t>::operator*() const noexcept
{
  const unsigned char c = static_cast<unsigned char>(*m_it);

  if (c < 0x80u) return c;
  else
  {
    auto it = m_it;
    char32_t d;

    if (c < 0xE0u)
      d = static_cast<char32_t>(c & 0x1Fu) << 6;
    else
    {
      if (c < 0xF0u)
        d = static_cast<char32_t>(c & 0xFu) << 12;
      else
      {
        d = static_cast<char32_t>(c & 0x7u) << 18;
        ++it;
        d |= static_cast<char32_t>(static_cast<unsigned char>(*it) & 0x3Fu) << 12;
      }

      ++it;
      d |= static_cast<char32_t>(static_cast<unsigned char>(*it) & 0x3Fu) << 6;
    }

    ++it;
    d |= static_cast<unsigned char>(*it) & 0x3Fu;

    return d;
  }
}


//
// BASIC STUFF
//

ctp::json::json(const json_type type): m_type{type}
{
  switch (m_type)
  {
  case json_type::null: break;
  case json_type::boolean: new(&m_union.m_boolean) json_boolean; break;
  case json_type::number: new(&m_union.m_number) json_number; break;
  case json_type::string: new(&m_union.m_string) json_string; break;
  case json_type::array: new(&m_union.m_array) json_array; break;
  case json_type::object: new(&m_union.m_object) json_object; break;
  }
}

ctp::json::json(const json &other): m_type{other.m_type}
{
  switch (m_type)
  {
  case json_type::null: break;
  case json_type::boolean: new(&m_union.m_boolean) json_boolean{other.m_union.m_boolean}; break;
  case json_type::number: new(&m_union.m_number) json_number{other.m_union.m_number}; break;
  case json_type::string: new(&m_union.m_string) json_string{other.m_union.m_string}; break;
  case json_type::array: new(&m_union.m_array) json_array(other.m_union.m_array); break;
  case json_type::object: new(&m_union.m_object) json_object(other.m_union.m_object); break;
  }
}

ctp::json::json(json &&other) noexcept: m_type{other.m_type}
{
  switch (m_type)
  {
  case json_type::null: break;
  case json_type::boolean: new(&m_union.m_boolean) json_boolean{std::move(other.m_union.m_boolean)}; break;
  case json_type::number: new(&m_union.m_number) json_number{std::move(other.m_union.m_number)}; break;
  case json_type::string: new(&m_union.m_string) json_string{std::move(other.m_union.m_string)}; break;
  case json_type::array: new(&m_union.m_array) json_array(std::move(other.m_union.m_array)); break;
  case json_type::object: new(&m_union.m_object) json_object(std::move(other.m_union.m_object)); break;
  }
}

ctp::json::~json()
{
  switch (m_type)
  {
  case json_type::null: break;
  case json_type::boolean: m_union.m_boolean.~json_boolean(); break;
  case json_type::number: m_union.m_number.~json_number(); break;
  case json_type::string: m_union.m_string.~json_string(); break;
  case json_type::array: m_union.m_array.~json_array(); break;
  case json_type::object: m_union.m_object.~json_object(); break;
  }
}

ctp::json &ctp::json::operator=(json_null) noexcept
{
  if (m_type != json_type::null)
  {
    this->~json();
    return *new(this) json;
  }

  return *this;
}

ctp::json &ctp::json::operator=(json_boolean other) noexcept
{
  if (m_type != json_type::boolean)
  {
    this->~json();
    return *new(this) json(std::move(other));
  }

  m_union.m_boolean = std::move(other);
  return *this;
}

ctp::json &ctp::json::operator=(const json_number &other)
{
  if (m_type != json_type::number) return (*this = json_number(other));

  m_union.m_number = other;
  return *this;
}

ctp::json &ctp::json::operator=(json_number &&other) noexcept
{
  if (m_type != json_type::number)
  {
    this->~json();
    return *new(this) json(std::move(other));
  }

  m_union.m_number = std::move(other);
  return *this;
}

ctp::json &ctp::json::operator=(const json_string &other)
{
  if (m_type != json_type::string) return (*this = json_string(other));

  m_union.m_string = other;
  return *this;
}

ctp::json &ctp::json::operator=(json_string &&other) noexcept
{
  if (m_type != json_type::string)
  {
    this->~json();
    return *new(this) json(std::move(other));
  }

  m_union.m_string = std::move(other);
  return *this;
}

ctp::json &ctp::json::operator=(const json_array &other)
{
  if (m_type != json_type::array) return (*this = json_array(other));

  m_union.m_array = other;
  return *this;
}

ctp::json &ctp::json::operator=(json_array &&other) noexcept
{
  if (m_type != json_type::array)
  {
    this->~json();
    return *new(this) json(std::move(other));
  }

  m_union.m_array = std::move(other);
  return *this;
}

ctp::json &ctp::json::operator=(const json_object &other)
{
  if (m_type != json_type::object) return (*this = json_object(other));

  m_union.m_object = other;
  return *this;
}

ctp::json &ctp::json::operator=(json_object &&other) noexcept
{
  if (m_type != json_type::object)
  {
    this->~json();
    return *new(this) json(std::move(other));
  }

  m_union.m_object = std::move(other);
  return *this;
}


ctp::json &ctp::json::operator=(const bool v) noexcept
{
  if (m_type != json_type::boolean)
  {
    this->~json();
    return *new(this) json(v);
  }

  m_union.m_boolean = v;
  return *this;
}

#define DEFN_NUMBER_CONV_ASSN(t) \
  ctp::json &ctp::json::operator=(const t v) \
  { if (m_type != json_type::number) { this->~json(); return *new(this) json(v); } \
    m_union.m_number = v; return *this; }

DEFN_NUMBER_CONV_ASSN(int)
DEFN_NUMBER_CONV_ASSN(long)
DEFN_NUMBER_CONV_ASSN(long long)
DEFN_NUMBER_CONV_ASSN(unsigned)
DEFN_NUMBER_CONV_ASSN(unsigned long)
DEFN_NUMBER_CONV_ASSN(unsigned long long)
DEFN_NUMBER_CONV_ASSN(float)
DEFN_NUMBER_CONV_ASSN(double)
DEFN_NUMBER_CONV_ASSN(long double)

#undef DEFN_NUMBER_CONV_ASSN

ctp::json &ctp::json::operator=(const char *const p)
{
  if (m_type != json_type::string)
  {
    this->~json();
    return *new(this) json(p);
  }

  m_union.m_string = p;
  return *this;
}

ctp::json &ctp::json::operator=(const std::string &v)
{
  if (m_type != json_type::string)
  {
    this->~json();
    return *new(this) json(v);
  }

  m_union.m_string = v;
  return *this;
}

ctp::json &ctp::json::operator=(std::string &&v) noexcept
{
  if (m_type != json_type::string)
  {
    this->~json();
    return *new(this) json(std::move(v));
  }

  m_union.m_string = std::move(v);
  return *this;
}


ctp::json &ctp::json::operator=(const json &other)
{
  if (m_type != other.m_type) return (*this = json{other});

  switch (m_type)
  {
  case json_type::null: break;
  case json_type::boolean: m_union.m_boolean = other.m_union.m_boolean; break;
  case json_type::number: m_union.m_number = other.m_union.m_number; break;
  case json_type::string: m_union.m_string = other.m_union.m_string; break;
  case json_type::array: m_union.m_array = other.m_union.m_array; break;
  case json_type::object: m_union.m_object = other.m_union.m_object; break;
  }

  return *this;
}

ctp::json &ctp::json::operator=(json &&other) noexcept
{
  if (m_type != other.m_type)
  {
    this->~json();
    return *new(this) json{std::move(other)};
  }

  switch (m_type)
  {
  case json_type::null: break;
  case json_type::boolean: m_union.m_boolean = std::move(other.m_union.m_boolean); break;
  case json_type::number: m_union.m_number = std::move(other.m_union.m_number); break;
  case json_type::string: m_union.m_string = std::move(other.m_union.m_string); break;
  case json_type::array: m_union.m_array = std::move(other.m_union.m_array); break;
  case json_type::object: m_union.m_object = std::move(other.m_union.m_object); break;
  }

  return *this;
}


template<typename T> T &ctp::get(json &j)
{
  if (j.m_type != json_type_alternative<T>::value) throw bad_json_access();
  return reinterpret_cast<T &>(j.m_union);
}

template<typename T> T &&ctp::get(json &&j)
{
  if (j.m_type != json_type_alternative<T>::value) throw bad_json_access();
  return std::move(reinterpret_cast<T &>(j.m_union));
}

template<typename T> const T &ctp::get(const json &j)
{
  if (j.m_type != json_type_alternative<T>::value) throw bad_json_access();
  return reinterpret_cast<const T &>(j.m_union);
}

template<typename T> const T &&ctp::get(const json &&j)
{
  if (j.m_type != json_type_alternative<T>::value) throw bad_json_access();
  return std::move(reinterpret_cast<const T &>(j.m_union));
}

template<typename T> T *ctp::get_if(json *const j) noexcept
{
  if (!j || j->m_type != json_type_alternative<T>::value) return nullptr;
  return reinterpret_cast<T *>(&j->m_union);
}

template<typename T> const T *ctp::get_if(const json *const j) noexcept
{
  if (!j || j->m_type != json_type_alternative<T>::value) return nullptr;
  return reinterpret_cast<const T *>(&j->m_union);
}

#define DEFN_ACCESSORS(t) \
  template ctp::json_##t &ctp::get<ctp::json_##t>(json &); \
  template ctp::json_##t &&ctp::get<ctp::json_##t>(json &&); \
  template const ctp::json_##t &ctp::get<ctp::json_##t>(const json &); \
  template const ctp::json_##t &&ctp::get<ctp::json_##t>(const json &&); \
  template ctp::json_##t *ctp::get_if<ctp::json_##t>(json *) noexcept; \
  template const ctp::json_##t *ctp::get_if<ctp::json_##t>(const json *) noexcept;

DEFN_ACCESSORS(null)
DEFN_ACCESSORS(boolean)
DEFN_ACCESSORS(number)
DEFN_ACCESSORS(string)
DEFN_ACCESSORS(array)
DEFN_ACCESSORS(object)

#undef DEFN_ACCESSOR


//
// NUMBER STUFF
//

namespace
{
  constexpr float logs[10] =
  {
    std::log2(1.0f), std::log2(2.0f), std::log2(3.0f), std::log2(4.0f), std::log2(5.0f),
    std::log2(6.0f), std::log2(7.0f), std::log2(8.0f), std::log2(9.0f), std::log2(10.0f)
  };

  constexpr float inv_log10 = std::log10(2.0f);

  template<typename intT> intT parse_int(const std::string::const_iterator begin, const std::string::const_iterator end, const int digits)
  {
    intT acc = 0;
    constexpr intT base = 10;
    int rem = digits;

    auto it = begin;

    if (*it == '\x2D')
    {
      ++it;

      for (; rem > 0 && it != end && *it != '\x65' && *it != '\x45'; ++it)
      {
        if (*it == '\x2E') continue;

        const intT d = *it - '\x30';

        if (acc < std::numeric_limits<intT>::min() / base ||
            acc * base < std::numeric_limits<intT>::min() + d)
          throw std::out_of_range("ctp::parse_int");

        acc = acc * base - d;
        --rem;
      }

      for (; rem > 0; --rem)
      {
        if (acc < std::numeric_limits<intT>::min() / base)
          throw std::out_of_range("ctp::parse_int");

        acc *= base;
      }
    }
    else
    {
      for (; rem > 0 && it != end && *it != '\x65' && *it != '\x45'; ++it)
      {
        if (*it == '\x2E') continue;

        const intT d = *it - '\x30';

        if (acc > std::numeric_limits<intT>::max() / base ||
            acc * base > std::numeric_limits<intT>::max() - d)
          throw std::out_of_range("ctp::parse_int");

        acc = acc * base + d;
        --rem;
      }

      for (; rem > 0; --rem)
      {
        if (acc > std::numeric_limits<intT>::max() / base)
          throw std::out_of_range("ctp::parse_int");

        acc *= base;
      }
    }

    return acc;
  }

  template<typename floatT> std::string format_float(const floatT v, const int digits)
  {
    if (digits < 1) throw std::invalid_argument("ctp::format_float");
    std::string ret;
    std::ostringstream oss;
    oss.imbue(std::locale::classic());
    oss << std::scientific;
    oss.precision(digits - 1);
    oss << v;
    return oss.str();
  }

  template<typename floatT> floatT parse_float(const std::string::const_iterator begin, const std::string::const_iterator end)
  {
    floatT ret;
    std::istringstream iss;
    std::ios::iostate state;
    std::use_facet<std::num_get<char, std::string::const_iterator>>(std::locale::classic()).get(
      begin, end, iss, state, ret);
    if (iss.fail())
    {
      if (ret == 0) std::terminate();
      else throw std::out_of_range("ctp::parse_float");
    }
    return ret;
  }
};

ctp::json_number::json_number(const float v, const int sig_digs): m_number{format_float(v, sig_digs)} { }

ctp::json_number::json_number(const double v, const int sig_digs): m_number{format_float(v, sig_digs)} { }

ctp::json_number::json_number(const long double v, const int sig_digs): m_number{format_float(v, sig_digs)} { }

ctp::json_number::json_number(const float v, bits_t, const int sig_bits)
  : json_number{v, static_cast<int>(std::ceil(1.0f + sig_bits * inv_log10))} { }

ctp::json_number::json_number(const double v, bits_t, const int sig_bits)
  : json_number{v, static_cast<int>(std::ceil(1.0f + sig_bits * inv_log10))} { }

ctp::json_number::json_number(const long double v, bits_t, const int sig_bits)
  : json_number{v, static_cast<int>(std::ceil(1.0f + sig_bits * inv_log10))} { }

ctp::json_number &ctp::json_number::operator=(const float v)
{ m_number = format_float(v, std::numeric_limits<float>::max_digits10); return *this; }

ctp::json_number &ctp::json_number::operator=(const double v)
{ m_number = format_float(v, std::numeric_limits<double>::max_digits10); return *this; }

ctp::json_number &ctp::json_number::operator=(const long double v)
{ m_number = format_float(v, std::numeric_limits<long double>::max_digits10); return *this; }

ctp::json_number::operator int() const
{ return parse_int<int>(m_number.begin(), m_number.end(), integral_digits(*this)); }

ctp::json_number::operator long() const
{ return parse_int<long>(m_number.begin(), m_number.end(), integral_digits(*this)); }

ctp::json_number::operator long long() const
{ return parse_int<long long>(m_number.begin(), m_number.end(), integral_digits(*this)); }

ctp::json_number::operator unsigned() const
{ return parse_int<unsigned>(m_number.begin(), m_number.end(), integral_digits(*this)); }

ctp::json_number::operator unsigned long() const
{ return parse_int<unsigned long>(m_number.begin(), m_number.end(), integral_digits(*this)); }

ctp::json_number::operator unsigned long long() const
{ return parse_int<unsigned long long>(m_number.begin(), m_number.end(), integral_digits(*this)); }

ctp::json_number::operator float() const
{ return parse_float<float>(m_number.begin(), m_number.end()); }

ctp::json_number::operator double() const
{ return parse_float<double>(m_number.begin(), m_number.end()); }

ctp::json_number::operator long double() const
{ return parse_float<long double>(m_number.begin(), m_number.end()); }

bool ctp::is_integral(const json_number &jn) noexcept
{
  std::string::const_iterator decimal = jn.m_number.end(), last_nonzero = jn.m_number.begin(), it;

  for (it = jn.m_number.begin(); it != jn.m_number.end() && *it != '\x65' && *it != '\x45'; ++it)
  {
    if (*it == '\x2E') decimal = it;
    if (*it >= '\x31' && *it <= '\x39') last_nonzero = it;
  }

  if (it == jn.m_number.end()) return last_nonzero <= decimal;
  if (last_nonzero == jn.m_number.begin()) return true;
  if (decimal == jn.m_number.end()) decimal = it;
  // assumption: we don't find more digits than the range of strtoll
  return std::strtoll(&*it, nullptr, 10) >= (last_nonzero - decimal) + (decimal > last_nonzero);
}

bool ctp::is_zero(const json_number &jn) noexcept
{
  for (auto it = jn.m_number.begin(); it != jn.m_number.end() && *it != '\x65' && *it != '\x45'; ++it)
    if (*it >= '\x31' && *it <= '\x39') return false;
  return true;
}

int ctp::significant_digits(const json_number &jn) noexcept
{
  int count = 0;

  auto it = jn.m_number.begin();
  for (; it != jn.m_number.end() && *it != '\x65' && *it != '\x45'; ++it)
    if (*it >= '\x31' && *it <= '\x39') { ++count; ++it; break; }

  for (; it != jn.m_number.end() && *it != '\x65' && *it != '\x45'; ++it)
    if (*it != '\x2E') ++count;

  return count;
}

int ctp::significant_bits(const json_number &jn) noexcept
{
  int first_digit = 0;

  auto it = jn.m_number.begin();
  for (; it != jn.m_number.end() && *it != '\x65' && *it != '\x45'; ++it)
    if (*it >= '\x31' && *it <= '\x39')
    {
      first_digit = *it - '\x30';
      ++it;
      break;
    }

  int count = 0;

  for (; it != jn.m_number.end() && *it != '\x65' && *it != '\x45'; ++it)
    if (*it != '\x2E') ++count;

  return std::ceil(logs[first_digit] + logs[9] * count);
}

int ctp::integral_digits(const json_number &jn) noexcept
{
  std::string::const_iterator decimal = jn.m_number.end(), first_nonzero = jn.m_number.end(), it;

  for (it = jn.m_number.begin(); it != jn.m_number.end() && *it != '\x65' && *it != '\x45'; ++it)
  {
    if (*it == '\x2E') decimal = it;
    if (it < first_nonzero && *it >= '\x31' && *it <= '\x39') first_nonzero = it;
  }

  if (it == jn.m_number.end()) return std::max(0, static_cast<int>(decimal - first_nonzero));
  if (first_nonzero == jn.m_number.end()) return 0;
  if (decimal == jn.m_number.end()) decimal = it;
  return std::max(0,
      std::atoi(&*it) + static_cast<int>(decimal - first_nonzero) + (first_nonzero > decimal));
}

int ctp::integral_bits(const json_number &jn) noexcept
{
  std::string::const_iterator decimal = jn.m_number.end(), first_nonzero = jn.m_number.end(), it;

  for (it = jn.m_number.begin(); it != jn.m_number.end() && *it != '\x65' && *it != '\x45'; ++it)
  {
    if (*it == '\x2E') decimal = it;
    if (it < first_nonzero && *it >= '\x31' && *it <= '\x39') first_nonzero = it;
  }

  if (it == jn.m_number.end()) return std::min(0, static_cast<int>(decimal - first_nonzero));
  if (first_nonzero == jn.m_number.end()) return 0;
  if (decimal == jn.m_number.end()) decimal = it;
  const int d =
    std::atoi(&*it) + static_cast<int>(decimal - first_nonzero) + (first_nonzero > decimal);
  if (d <= 0) return 0;
  return std::ceil(logs[*first_nonzero - '\x30'] + logs[9] * (d - 1));
}


//
// STRING STUFF
//

template<typename charT> void ctp::json_string_ops<charT>::push_back(const charT c) const
{
  if (c < 0x80u) m_json_string.m_string.push_back(c);
  else
  {
    if (c < 0x800u)
      m_json_string.m_string.push_back(0xC0u | (c >> 6));
    else
    {
      if (c < 0x10000u)
        m_json_string.m_string.push_back(0xE0u | (c >> 12));
      else
      {
        m_json_string.m_string.push_back(0xF0u | (c >> 18));
        m_json_string.m_string.push_back(0x80u | ((c >> 12) & 0x3Fu));
      }

      m_json_string.m_string.push_back(0x80u | ((c >> 6) & 0x3Fu));
    }

    m_json_string.m_string.push_back(0x80u | (c & 0x3Fu));
  }
}

// char instantiation is specialized
template void ctp::json_string_ops<unsigned char>::push_back(unsigned char) const;
//template void ctp::json_string_ops<wchar_t>::push_back(wchar_t) const;  // TODO (utf8_iterator)
template void ctp::json_string_ops<char16_t>::push_back(char16_t) const;
template void ctp::json_string_ops<char32_t>::push_back(char32_t) const;


//
// OBJECT STUFF
//

ctp::json_object &ctp::json_object::operator=(const json_object &other)
{
  using std::begin;
  using std::end;
  // This is needed because our elements aren't assignable.
  std::copy(begin(other.m_object), end(other.m_object), std::back_inserter(m_object));
  return *this;
}


//
// SERIALIZATION
//

namespace ctp
{
  static int g_ios_flags_idx = std::ios_base::xalloc();

  static constexpr int ascii_only = 1;
  static constexpr int bmp_only = 2;
  static constexpr int javascript_compat = 4;
  static constexpr int printable_only = 8;
  static constexpr int reject_duplicate_keys = 16;

  static char encode_hex(const int v)
  { return '\x30' + v + static_cast<int>(v >= 10) * 7; }

  template<typename charT>
  static std::basic_ostream<charT> &serialize_bmp2_seq(std::basic_ostream<charT> &os, std::string::const_iterator &it)
  {
    const unsigned char c = static_cast<unsigned char>(*it);
    const unsigned char d = static_cast<unsigned char>(*++it);
    const charT u[] = {'\x5C', '\x75', '\x30',
      static_cast<charT>(encode_hex((c >> 2) & 15u)), static_cast<charT>(encode_hex(((c & 3u) << 2) | ((d >> 4) & 3u))),
      static_cast<charT>(encode_hex(d & 15u))};
    return os.write(u, 6);
  }

  template<typename charT>
  static std::basic_ostream<charT> &serialize_bmp3_seq(std::basic_ostream<charT> &os, std::string::const_iterator &it)
  {
    const unsigned char c = static_cast<unsigned char>(*it);
    const unsigned char d = static_cast<unsigned char>(*++it);
    const unsigned char e = static_cast<unsigned char>(*++it);
    const charT u[] = {'\x5C', '\x75',
      static_cast<charT>(encode_hex(c & 15u)), static_cast<charT>(encode_hex((d >> 2) & 15u)),
      static_cast<charT>(encode_hex(((d & 3u) << 2) | ((e >> 4) & 3u))), static_cast<charT>(encode_hex(e & 15u))};
    return os.write(u, 6);
  }

  template<typename charT>
  static std::basic_ostream<charT> &serialize_nonbmp_seq(std::basic_ostream<charT> &os, std::string::const_iterator &it)
  {
    const unsigned char c = static_cast<unsigned char>(*it);
    const unsigned char d = static_cast<unsigned char>(*++it);
    const unsigned char e = static_cast<unsigned char>(*++it);
    const unsigned char f = static_cast<unsigned char>(*++it);
    const unsigned char cd = ((c & 7u) << 4) + ((d >> 2) & 15u) - 4u;
    const charT u[] = {'\x5C', '\x75', '\x44',
      static_cast<charT>(encode_hex(8u + (cd >> 4))),
      static_cast<charT>(encode_hex(cd & 15u)),
      static_cast<charT>(encode_hex(((d & 3u) << 2) | ((e >> 4) & 3u))),
      '\x5C', '\x75', '\x44',
      static_cast<charT>(encode_hex(12u + ((e >> 2) & 3u))),
      static_cast<charT>(encode_hex(((e & 3u) << 2) | ((f >> 4) & 3u))),
      static_cast<charT>(encode_hex(f & 15u))};
    return os.write(u, 12);
  }

  template<typename charT>
  static std::basic_ostream<charT> &serialize_char_aux(std::basic_ostream<charT> &os, std::string::const_iterator &it)
  {
    const unsigned char c = static_cast<unsigned char>(*it);
    charT cc;
    if (c < 0x80u) cc = c;
    else
    {
      if (c < 0xE0u) cc = c & 0x1Fu;
      else
      {
        if (c < 0xF0u) cc = c & 0x0Fu;
        else cc = ((c & 7u) << 6) | (static_cast<unsigned char>(*++it) & 0x3Fu);
        cc = (cc << 6) | (static_cast<unsigned char>(*++it) & 0x3Fu);
      }
      cc = (cc << 6) | (static_cast<unsigned char>(*++it) & 0x3Fu);
    }
    return os.put(cc);
  }

  template<> std::ostream &serialize_char_aux(std::ostream &os, std::string::const_iterator &it)
  {
    return os.put(*it);
  }

  template<> std::basic_ostream<char16_t> &serialize_char_aux(std::basic_ostream<char16_t> &os, std::string::const_iterator &it)
  {
    const unsigned char c = static_cast<unsigned char>(*it);
    char16_t cc;
    if (c < 0x80u) cc = c;
    else
    {
      if (c < 0xE0u) cc = c & 0x1Fu;
      else cc = ((c & 0x0Fu) << 6) | (static_cast<unsigned char>(*++it) & 0x3Fu);
      cc = (cc << 6) | (static_cast<unsigned char>(*++it) & 0x3Fu);
    }
    return os.put(cc);
  }

  template<typename charT>
  static std::basic_ostream<charT> &serialize_char(std::basic_ostream<charT> &os, std::string::const_iterator &it, const int flags)
  {
    // cases are structured to minimize branching for common cases

    const unsigned char c = static_cast<unsigned char>(*it);

    if (c < 0x20u)
    {
      switch (c)
      {
      case 8u: { static const charT u[] = {'\x5C', '\x62'}; return os.write(u, 2); }
      case 9u: { static const charT u[] = {'\x5C', '\x74'}; return os.write(u, 2); }
      case 10u: { static const charT u[] = {'\x5C', '\x6E'}; return os.write(u, 2); }
      case 12u: { static const charT u[] = {'\x5C', '\x66'}; return os.write(u, 2); }
      case 13u: { static const charT u[] = {'\x5C', '\x72'}; return os.write(u, 2); }
      default:
        {
          const charT u[] = {'\x5C', '\x75', '\x30', '\x30', static_cast<charT>(encode_hex(c >> 4)), static_cast<charT>(encode_hex(c & 15u))};
          return os.write(u, 6);
        }
      }
    }

    switch (c)
    {
    case 0x22u: { static const charT u[] = {'\x5C', '\x22'}; return os.write(u, 2); }
    case 0x5Cu: { static const charT u[] = {'\x5C', '\x5C'}; return os.write(u, 2); }

    case 0x7Fu:
      if ((flags & printable_only) != 0)
      {
        static const charT u[] = {'\x5C', '\x75', '\x30', '\x30', '\x37', '\x46'};
        return os.write(u, 6);
      }
      break;

    #define BMP2_LEAD(c) (((c) >> 6) | 0xC0u)
    #define BMP3_LEAD(c) (((c) >> 12) | 0xE0u)
    #define NONBMP_LEAD(c) (((c) >> 18) | 0xF0u)
    #define TAIL1(c) (((c) & 0x3Fu) | 0x80u)
    #define TAIL2(c) ((((c) >> 6) & 0x3Fu) | 0x80u)
    #define TAIL3(c) ((((c) >> 12) & 0x3Fu) | 0x80u)

    case BMP2_LEAD(0x80u):
      // U+0080..009F; U+00AD
      if ((flags & printable_only) != 0)
      {
        const unsigned char d = static_cast<unsigned char>(it[1]);
        if (d <= TAIL1(0x9Fu) || d == TAIL1(0xADu))
          return serialize_bmp2_seq(os, it);
      }
      break;

    case BMP2_LEAD(0x34Fu):
      // U+034F
      if ((flags & printable_only) != 0 &&
          static_cast<unsigned char>(it[1]) == TAIL1(0x34Fu))
        return serialize_bmp2_seq(os, it);
      break;

    case BMP2_LEAD(0x61Cu):
      // U+061C
      if ((flags & printable_only) != 0 &&
          static_cast<unsigned char>(it[1]) == TAIL1(0x61Cu))
        return serialize_bmp2_seq(os, it);
      break;

    case BMP3_LEAD(0x115Fu):
      // U+115F..1160; U+17B4..17B5; U+180B..180E
      if ((flags & printable_only) != 0)
      {
        const unsigned char d = static_cast<unsigned char>(it[1]);
        if (d == TAIL2(0x115Fu))
        {
          const unsigned char e = static_cast<unsigned char>(it[2]);
          if (e == TAIL1(0x115Fu) || e == TAIL1(0x1160u))
            return serialize_bmp3_seq(os, it);
        }
        else if (d == TAIL2(0x17B4u))
        {
          if ((static_cast<unsigned char>(it[2]) & 0xFEu) == TAIL1(0x17B4u))
            return serialize_bmp3_seq(os, it);
        }
        else if (d == TAIL2(0x180Bu))
        {
          const unsigned char e = static_cast<unsigned char>(it[2]);
          if (e >= TAIL1(0x180Bu) && e <= TAIL1(0x180Eu))
            return serialize_bmp3_seq(os, it);
        }
      }
      break;

    case BMP3_LEAD(0x200Bu):
      // U+200B..200F; U+2028..2029; U+202A..202E; U+2060..206F
      {
        const unsigned char d = static_cast<unsigned char>(it[1]);
        const unsigned char e = static_cast<unsigned char>(it[2]);
        if ((flags & printable_only) != 0)
        {
          if (d == TAIL2(0x200Bu))
          {
            if ((e >= TAIL1(0x200Bu) && e <= TAIL1(0x200Fu)) ||
                (((flags & javascript_compat) == 0 ? e >= TAIL1(0x202Au) : e >= TAIL1(0x2028u)) &&
                  e <= TAIL1(0x202Eu)))
              return serialize_bmp3_seq(os, it);
          }
          else if (d == TAIL2(0x2060u) && (e & 0xF0u) == TAIL1(0x2060u))
            return serialize_bmp3_seq(os, it);
        }
        else if ((flags & javascript_compat) != 0 && d == TAIL2(0x2028u) &&
            (e & 0xFEu) == TAIL1(0x2028u))
          return serialize_bmp3_seq(os, it);
      }
      break;

    case BMP3_LEAD(0x3164u):
      // U+3164
      if ((flags & printable_only) != 0 &&
          static_cast<unsigned char>(it[1]) == TAIL2(0x3164u) &&
          static_cast<unsigned char>(it[2]) == TAIL1(0x3164u))
        return serialize_bmp3_seq(os, it);
      break;

    case BMP3_LEAD(0xFE00u):
      // U+FE00..FE0F; U+FEFF; U+FFA0; U+FFF0..FFF8
      if ((flags & printable_only) != 0)
      {
        const unsigned char d = static_cast<unsigned char>(it[1]);
        if (d == TAIL2(0xFE00u))
        {
          if ((static_cast<unsigned char>(it[2]) & 0xF0u) == TAIL1(0xFE00u))
            return serialize_bmp3_seq(os, it);
        }
        else if (d == TAIL2(0xFFF0u))
        {
          const unsigned char e = static_cast<unsigned char>(it[2]);
          if (e >= TAIL1(0xFFF0u) && e <= TAIL1(0xFFF8u))
            return serialize_bmp3_seq(os, it);
        }
        else if (d == TAIL2(0xFEFFu))
        {
          if (static_cast<unsigned char>(it[2]) == TAIL1(0xFEFFu))
            return serialize_bmp3_seq(os, it);
        }
        else if (d == TAIL2(0xFFA0u))
        {
          if (static_cast<unsigned char>(it[2]) == TAIL1(0xFFA0u))
            return serialize_bmp3_seq(os, it);
        }
      }
      break;

    case NONBMP_LEAD(0x1BCA0u):
      // U+1BCA0..1BCA3; U+1D173..1D17A
      if ((flags & printable_only) != 0 &&
          static_cast<unsigned char>(it[1]) == TAIL3(0x1BCA0u))
      {
        const unsigned char e = static_cast<unsigned char>(it[2]);
        if (e == TAIL2(0x1BCA0u))
        {
          const unsigned char f = static_cast<unsigned char>(it[3]);
          if ((f & 0xFCu) == TAIL1(0x1BCA0u))
            return serialize_nonbmp_seq(os, it);
        }
        else if (e == TAIL2(0x1D173u))
        {
          const unsigned char f = static_cast<unsigned char>(it[3]);
          if (f >= TAIL1(0x1D173u) && f <= TAIL1(0x1D17Au))
            return serialize_nonbmp_seq(os, it);
        }
      }
      break;

    case NONBMP_LEAD(0xE0000u):
      // U+E0000..E0FFF
      if ((flags & printable_only) != 0 && static_cast<unsigned char>(it[1]) == TAIL3(0xE0000u))
        return serialize_nonbmp_seq(os, it);
      break;

    #undef BMP2_LEAD
    #undef BMP3_LEAD
    #undef NONBMP_LEAD
    #undef TAIL1
    #undef TAIL2
    #undef TAIL3

    default: break;
    }

    return serialize_char_aux(os, it);
  }

  template<typename charT>
  static std::basic_ostream<charT> &serialize_string(std::basic_ostream<charT> &os, const std::string &s)
  {
    const int flags = os.iword(g_ios_flags_idx);
    os.put('\x22');

    if ((!std::is_same<charT, char>::value && std::numeric_limits<charT>::max() < 0xFFFFu) ||
        (flags & ascii_only) != 0)
    {
      for (auto it = s.begin(); it != s.end(); ++it)
      {
        const unsigned char c = static_cast<unsigned char>(*it);
        if (c < 0x80u) serialize_char(os, it, flags);
        else if (c < 0xE0u) serialize_bmp2_seq(os, it);
        else if (c < 0xF0u) serialize_bmp3_seq(os, it);
        else serialize_nonbmp_seq(os, it);
      }
    }
    else if ((!std::is_same<charT, char>::value && std::numeric_limits<charT>::max() < 0x10FFFFu) ||
        (flags & bmp_only) != 0)
    {
      for (auto it = s.begin(); it != s.end(); ++it)
      {
        if (static_cast<unsigned char>(*it) < 0xF0u) serialize_char(os, it, flags);
        else serialize_nonbmp_seq(os, it);
      }
    }
    else
      for (auto it = s.begin(); it != s.end(); ++it)
        serialize_char(os, it, flags);

    os.put('\x22');
    return os;
  }

  template<typename charT>
  static std::basic_ostream<charT> &serialize_number(std::basic_ostream<charT> &os, const std::string &s)
  {
    for (const char c: s) os.put(c);
    return os;
  }

  template<> std::ostream &serialize_number(std::ostream &os, const std::string &s)
  {
    return os.write(s.data(), s.size());
  }
};

template<typename charT>
std::basic_ostream<charT> &ctp::operator<<(std::basic_ostream<charT> &os, const json &j)
{
  // FIXME: implement w/o recursion

  switch (j.type())
  {
  case json_type::null:
    {
      static const charT u[] = {'\x6E', '\x75', '\x6C', '\x6C'};
      return os.write(u, 4);
    }

  case json_type::boolean:
    if (get<json_boolean>(j))
    {
      static const charT u[] = {'\x74', '\x72', '\x75', '\x65'};
      return os.write(u, 4);
    }
    else
    {
      static const charT u[] = {'\x66', '\x61', '\x6C', '\x73', '\x65'};
      return os.write(u, 5);
    }

  case json_type::number:
    return serialize_number(os, get<json_number>(j).m_number);

  case json_type::string:
    return serialize_string(os, get<json_string>(j));

  case json_type::array:
    {
      const json_array &array = get<json_array>(j);
      os.put('\x5B');
      if (!array.empty())
      {
        auto it = array.begin();
        os << *it;
        for (++it; it != array.end(); ++it)
        {
          os.put('\x2C');
          os << *it;
        }
      }
      os.put('\x5D');
      break;
    }

  case json_type::object:
    {
      const json_object &object = get<json_object>(j);
      os.put('\x7B');
      if (!object.empty())
      {
        auto it = object.begin();
        serialize_string(os, it->first);
        os.put('\x3A');
        os << it->second;
        for (++it; it != object.end(); ++it)
        {
          os.put('\x2C');
          serialize_string(os, it->first);
          os.put('\x3A');
          os << it->second;
        }
      }
      os.put('\x7D');
      break;
    }
  }

  return os;
}

template std::ostream &ctp::operator<<(std::ostream &, const json &);
template std::wostream &ctp::operator<<(std::wostream &, const json &);
template std::basic_ostream<char16_t> &ctp::operator<<(std::basic_ostream<char16_t> &, const json &);
template std::basic_ostream<char32_t> &ctp::operator<<(std::basic_ostream<char32_t> &, const json &);

std::ios_base &ctp::json_ascii(std::ios_base &s) noexcept
{ s.iword(g_ios_flags_idx) |= ascii_only; return s; }

std::ios_base &ctp::json_bmp(std::ios_base &s) noexcept
{ long &w = s.iword(g_ios_flags_idx); w = (w & ~ascii_only) | bmp_only; return s; }

std::ios_base &ctp::json_unicode(std::ios_base &s) noexcept
{ s.iword(g_ios_flags_idx) &= ~(ascii_only | bmp_only); return s; }

std::ios_base &ctp::json_javascript_compat(std::ios_base &s) noexcept
{ s.iword(g_ios_flags_idx) |= javascript_compat; return s; }

std::ios_base &ctp::json_no_javascript_compat(std::ios_base &s) noexcept
{ s.iword(g_ios_flags_idx) &= ~javascript_compat; return s; }

std::ios_base &ctp::json_printable_only(std::ios_base &s) noexcept
{ s.iword(g_ios_flags_idx) |= printable_only; return s; }

std::ios_base &ctp::json_no_printable_only(std::ios_base &s) noexcept
{ s.iword(g_ios_flags_idx) &= ~printable_only; return s; }

std::ios_base &ctp::json_reject_duplicate_keys(std::ios_base &s) noexcept
{ s.iword(g_ios_flags_idx) |= reject_duplicate_keys; return s; }

std::ios_base &ctp::json_no_reject_duplicate_keys(std::ios_base &s) noexcept
{ s.iword(g_ios_flags_idx) &= ~reject_duplicate_keys; return s; }


//
// DESERIALIZATION
//

namespace ctp
{
  // TODO: FIXME: use char_traits::to_char_type instead of casts from peek

  template<typename charT> static bool is_hex(const charT hex)
  {
    return (hex >= '\x30' && hex <= '\x39') ||
      (hex >= '\x41' && hex <= '\x46') || 
      (hex >= '\x61' && hex <= '\x66');
  }

  template<typename charT> static unsigned int decode_hex(const charT hex)
  { return (static_cast<unsigned int>(hex) & 15u) + static_cast<unsigned int>(hex >= '\x40') * 9u; }

  template<typename charT>
  static std::basic_istream<charT> &json_ws(std::basic_istream<charT> &is)
  {
    for (;;)
    {
      switch (is.peek())
      {
      case '\x09': case '\x0A': case '\x0D': case '\x20':
        is.ignore(); break;

      default:
        return is;
      }
    }
  }

  template<typename charT>
  static std::basic_istream<charT> &check_string(std::basic_istream<charT> &is, const char *s)
  {
    for (; *s != '\0'; ++s)
    {
      if (is.peek() != static_cast<typename std::char_traits<charT>::int_type>(*s))
      {
        is.setstate(std::istream::failbit);
        return is;
      }

      is.ignore();
    }

    return is;
  }

  template<typename charT>
  static void encode_utf8(const charT c, std::string &s)
  {
    const typename std::char_traits<charT>::int_type ci = c;

    if (ci < 0x80)
      s.push_back(static_cast<char>(c));
    else
    {
      if (ci < 0x800)
        s.push_back(static_cast<char>(0xC0 | (c >> 6)));
      else
      {
        if (ci < 0x10000)
          s.push_back(static_cast<char>(0xE0 | (c >> 12)));
        else
        {
          s.push_back(static_cast<char>(0xF0 | (c >> 18)));
          s.push_back(static_cast<char>(0x80 | ((c >> 12) & 0x3F)));
        }

        s.push_back(static_cast<char>(0x80 | ((c >> 6) & 0x3F)));
      }

      s.push_back(static_cast<char>(0x80 | (c & 0x3F)));
    }
  }

  template<typename charT>
  static std::basic_istream<charT> &deserialize_char(const charT c, std::basic_istream<charT> &is, std::string &s)
  {
    charT cc;

    if (c == '\x5C')
    {
      is.ignore();
      const charT d = is.peek();
      switch (d)
      {
      case '\x22': case '\x2F': case '\x5C':
        is.ignore(); cc = d; break;

      case '\x62': is.ignore(); cc = '\x08'; break;
      case '\x66': is.ignore(); cc = '\x0C'; break;
      case '\x6E': is.ignore(); cc = '\x0A'; break;
      case '\x72': is.ignore(); cc = '\x0D'; break;
      case '\x74': is.ignore(); cc = '\x09'; break;

      case '\x75':
        {
          is.ignore();

          const charT e = is.peek();
          if (!is_hex(e)) goto fail;
          const unsigned int ee = decode_hex(e);
          is.ignore();

          const charT f = is.peek();
          if (!is_hex(f)) goto fail;
          const unsigned int ff = decode_hex(f);

          char32_t u;

          if (ee == 13u && ff >= 8u)
          {
            // surrogate pair
            if (ff >= 12u) goto fail;
            is.ignore();
            u = ff - 8u;
            for (int i = 0; i < 2; i++)
            {
              const charT g = is.peek();
              if (!is_hex(g)) goto fail;
              u = (u << 4) + decode_hex(g);
              is.ignore();
            }

            if (is.peek() != '\x5C') goto fail;
            is.ignore();
            if (is.peek() != '\x75') goto fail;
            is.ignore();
            const charT h = is.peek();
            if (h != '\x44' && h != '\x64') goto fail;
            is.ignore();
            const charT l = is.peek();
            if (!is_hex(l)) goto fail;
            const unsigned int ll = decode_hex(l);
            if (ll < 12u) goto fail;
            is.ignore();
            u = (u << 2) + ll + 0xF4u;
          }
          else
          {
            // normal escape
            is.ignore();
            u = (static_cast<char32_t>(ee) << 4) + ff;
          }

          for (int i = 0; i < 2; i++)
          {
            const charT g = is.peek();
            if (!is_hex(g)) goto fail;
            u = (u << 4) + decode_hex(g);
            is.ignore();
          }

          encode_utf8(u, s);
          return is;
        }

      default: goto fail;
      }
    }
    else
    {
      const typename std::char_traits<charT>::int_type ci = c;
      if (ci < 0x20 || (c >= 0xD800 && c < 0xE000) || c >= 0x110000) goto fail;
      is.ignore();
      cc = c;
    }

    // use charT rather than char32_t here (as we do for \u escapes) to save cycles for the common case
    // for UTF-8 and UTF-16 text
    encode_utf8(cc, s);
    return is;

  fail:
    is.setstate(std::istream::failbit);
    return is;
  }

  template<typename charT>
  static std::basic_istream<charT> &deserialize_string(std::basic_istream<charT> &is, std::string &s)
  {
    for (;;)
    {
      const auto c = is.peek();
      if (c == '\x22') { is.ignore(); return is; }
      // need to confirm EOF since char16_t EOF is a valid code point
      if (c == std::basic_istream<charT>::traits_type::eof() && is.fail()) goto fail;
      if (!deserialize_char(static_cast<charT>(c), is, s)) return is;
    }

  fail:
    is.setstate(std::istream::failbit);
    return is;
  }

  template<> std::istream &deserialize_string(std::istream &is, std::string &s)
  {
    for (;;)
    {
      const auto c = is.peek();
      if (c == '\x22') { is.ignore(); return is; }
      if (c == std::istream::traits_type::eof()) goto fail;
      const unsigned char cc = static_cast<unsigned char>(c);
      if (cc < 0x80u)
      {
        if (!deserialize_char(static_cast<char>(cc), is, s)) return is;
      }
      else
      {
        if (cc < 0xC2u) goto fail;

        s.push_back(static_cast<char>(cc));

        if (cc < 0xE0u) is.ignore();
        else
        {
          if (cc < 0xF0u) is.ignore();
          else
          {
            if (cc > 0xF4u) goto fail;
            is.ignore();

            const auto d = is.peek();
            if (d == std::istream::traits_type::eof()) goto fail;
            const unsigned char dd = static_cast<unsigned char>(d);
            if ((dd & 0xC0u) != 0x80u ||
                (cc == 0xF0u && dd < 0x90u) ||
                (cc == 0xF4u && dd >= 0x90u)) goto fail;
            s.push_back(static_cast<char>(dd));
            is.ignore();
          }

          const auto d = is.peek();
          if (d == std::istream::traits_type::eof()) goto fail;
          const unsigned char dd = static_cast<unsigned char>(d);
          if ((dd & 0xC0u) != 0x80u ||
            (cc == 0xE0u && dd < 0xA0u)) goto fail;
          s.push_back(static_cast<char>(dd));
          is.ignore();
        }

        const auto d = is.peek();
        if (d == std::istream::traits_type::eof()) goto fail;
        const unsigned char dd = static_cast<unsigned char>(d);
        if ((dd & 0xC0u) != 0x80u) goto fail;
        s.push_back(static_cast<char>(dd));
        is.ignore();
      }
    }

  fail:
    is.setstate(std::istream::failbit);
    return is;
  }
};

template<typename charT>
std::basic_istream<charT> &ctp::operator>>(std::basic_istream<charT> &is, json &j)
{
  j = json_null();

  json ret;
  const int flags = is.iword(g_ios_flags_idx);
  std::stack<json *> stack;
  std::stack<std::unordered_set<std::string>> dup_keys_stack;
  json *cur = &ret;

do_ws_elt:
  // At (possible) whitespace before value.
  json_ws(is);
  
do_elt:
  // At start of value; will place in *cur.
  {
    auto c = is.peek();
    switch (c)
    {
    case '\x5B':
      {
        is.ignore();
        *cur = json_array();

        json_ws(is);
        if (is.peek() == '\x5D')
        {
          is.ignore();
          goto next_elt;
        }
        else
        {
          json_array &arr = get<json_array>(*cur);
          stack.push(cur);
          arr.emplace_back();
          cur = &arr.back();
          goto do_elt;
        }
      }

    case '\x7B':
      {
        is.ignore();
        *cur = json_object();

        json_ws(is);
        if (is.peek() == '\x22')
        {
          is.ignore();
          std::string s;
          if (!deserialize_string(is, s)) return is;

          if ((flags & reject_duplicate_keys) != 0)
          {
            dup_keys_stack.emplace();
            dup_keys_stack.top().insert(s);
          }

          json_object &obj = get<json_object>(*cur);
          stack.push(cur);
          cur = &obj.emplace(std::move(s), json_null())->second;

          json_ws(is);
          if (is.peek() != '\x3A') goto fail;
          is.ignore();
          goto do_ws_elt;
        }
        else if (is.peek() == '\x7D')
        {
          is.ignore();
          goto next_elt;
        }
        else goto fail;
      }

    case '\x22':
      {
        is.ignore();
        std::string s;
        if (!deserialize_string(is, s)) return is;
        *cur = std::move(s);
        goto next_elt;
      }

    case '\x74':
      is.ignore();
      if (!check_string(is, "\x72\x75\x65")) return is;
      *cur = true;
      goto next_elt;

    case '\x66':
      is.ignore();
      if (!check_string(is, "\x61\x6C\x73\x65")) return is;
      *cur = false;
      goto next_elt;

    case '\x6E':
      is.ignore();
      if (!check_string(is, "\x75\x6C\x6C")) return is;
      *cur = json_null();
      goto next_elt;

    default:
      if (c == '\x2D' || (c >= '\x30' && c <= '\x39'))
      {
        std::string s;

        if (c == '\x2D')
        {
          s.push_back(c);
          is.ignore();
          c = is.peek();
        }

        if (c == '\x30')
        {
          s.push_back(c);
          is.ignore();
          c = is.peek();
        }
        else
        {
          if (c < '\x31' || c > '\x39') goto fail;
          s.push_back(c);
          is.ignore();
          for (c = is.peek(); c >= '\x30' && c <= '\x39'; c = is.peek())
          {
            s.push_back(c);
            is.ignore();
          }
        }

        if (c == '\x2E')
        {
          s.push_back(c);
          is.ignore();
          c = is.peek();
          if (c < '\x30' || c > '\x39') goto fail;
          do
          {
            s.push_back(c);
            is.ignore();
            c = is.peek();
          } while (c >= '\x30' && c <= '\x39');
        }

        if (c == '\x65' || c == '\x45')
        {
          s.push_back(c);
          is.ignore();
          c = is.peek();
          if (c == '\x2B' || c == '\x2D')
          {
            s.push_back(c);
            is.ignore();
            c = is.peek();
          }
          if (c < '\x30' || c > '\x39') goto fail;
          do
          {
            s.push_back(c);
            is.ignore();
            c = is.peek();
          } while (c >= '\x30' && c <= '\x39');
        }

        // FIXME: ensure that number length + exponent is within bounds
        *cur = json_number(std::move(s));
        goto next_elt;
      }
      else goto fail;
    }
  }

next_elt:
  // At end of value.
  if (stack.empty())
  {
    j = std::move(ret);
    return is;
  }
  else
  {
    // Value was part of aggregate; skip whitespace, then continue or close aggregate.
    json_ws(is);
    const char d = is.peek();

    switch (stack.top()->type())
    {
    case json_type::array:
      {
        json_array &arr = get<json_array>(*stack.top());
        if (d == '\x2C')
        {
          is.ignore();
          arr.emplace_back();
          cur = &arr.back();
          goto do_ws_elt;
        }
        else if (d == '\x5D')
        {
          is.ignore();
          stack.pop();
          goto next_elt;
        }
        else goto fail;
      }

    case json_type::object:
      {
        json_object &obj = get<json_object>(*stack.top());
        if (d == '\x2C')
        {
          is.ignore();
          json_ws(is);
          if (is.peek() != '\x22') goto fail;
          is.ignore();
          std::string s;
          if (!deserialize_string(is, s)) return is;

          if ((flags & reject_duplicate_keys) != 0 &&
              !dup_keys_stack.top().insert(s).second)
            goto fail;

          cur = &obj.emplace(std::move(s), json_null())->second;

          json_ws(is);
          if (is.peek() != '\x3A') goto fail;
          is.ignore();
          goto do_ws_elt;
        }
        else if (d == '\x7D')
        {
          is.ignore();
          stack.pop();
          if ((flags & reject_duplicate_keys) != 0)
            dup_keys_stack.pop();
          goto next_elt;
        }
        else goto fail;
      }

    default: std::terminate();
    }
  }

fail:
  is.setstate(std::istream::failbit);
  return is;
}

template std::istream &ctp::operator>>(std::istream &, json &);
template std::wistream &ctp::operator>>(std::wistream &, json &);
//template std::basic_istream<char16_t> &ctp::operator>>(std::basic_istream<char16_t> &, json &);
//template std::basic_istream<char32_t> &ctp::operator>>(std::basic_istream<char32_t> &, json &);
