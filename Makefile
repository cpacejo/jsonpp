CPPFLAGS = -std=c++11 -Wall -Wextra -Werror -Os -DNDEBUG

OBJS = json.o

jsonpp.a: $(OBJS)
	ar rcs $@ $(OBJS)

json.o: json.h
